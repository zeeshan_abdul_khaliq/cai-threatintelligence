import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TipComponent } from './tip.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {RiskFactorListingComponent} from './riskFactorListing/riskFactorListing.component';
import {ExploreComponent} from './explore/explore.component';
import {ReportPanelComponent} from './Report Panel/reportPanel.component';
import {TopStatisticsComponent} from './Top Statistics/topStatistics.component';
import {FeedsComponent} from './feeds/feeds.component';
import {ReportListComponent} from './Report Panel/report-list/reportList.component';
import {SearchIocComponent} from './searchIoc/searchIoc.component';
import {Ipv4Component} from './dashboard/weekly-history-details/ipv4/ipv4.component';
import {UrlDomainComponent} from './dashboard/weekly-history-details/url-domain/url-domain.component';
import {OtherIocComponent} from './dashboard/weekly-history-details/other-ioc/other-ioc.component';
import {HashesMalComponent} from './dashboard/weekly-history-details/hashes-mal/hashes-mal.component';

const routes: Routes = [
  {
    path: '',
    component: TipComponent,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'explore',
        component: ExploreComponent
      },
      {
        path: 'reports',
        component: ReportPanelComponent
      },
      {
        path: 'reportList',
        component: ReportListComponent
      },
      {
        path: 'top-stats',
        component: TopStatisticsComponent
      },
      {
        path: 'feeds',
        component: FeedsComponent
      },
      {
        path: 'riskFactor',
        component: RiskFactorListingComponent
      },
      {
        path: 'search-ioc',
        component: SearchIocComponent
      },
      {
        path: 'history-ipv4',
        component: Ipv4Component
      },
      {
        path: 'history-others',
        component: OtherIocComponent
      },
      {
        path: 'history-domains-urls',
        component: UrlDomainComponent
      },
      {
        path: 'history-malware-hashes',
        component: HashesMalComponent
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TipRoutingModule { }
