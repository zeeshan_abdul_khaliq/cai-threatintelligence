import {Component, Input, OnChanges, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {MenuItem} from 'primeng/api';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import {TipService} from '../../core-services/tip.service';

@Component({
    selector: 'app-ioc-details',
    templateUrl: './ioc-details.component.html',
    styleUrls: ['./ioc-details.component.scss']
})
export class IocDetailsComponent implements OnInit, OnChanges {
    aptData: any;
    showFactorTab = false;
    showTab: any = false;
    subscribenmalware: any;
    filesToUpload: Set<File> = new Set();
    file: File | undefined | undefined;
    loader = false;
    isellect = true;
    export_loading = false;
    export_reporting = false;
    export_reporting_stix = false;
    detect;
    searchAttack: any;
    p: any;
    show: any;
    content = '';
    data: any;
    ifnoData = false;
    datareport: any;
    reporttype: any;
    downloadType: any;
    response: any;
    errors: any;
    yaraDetails: any;
    emailDetails: any;
    cveDetails: any;
    cvedetection = [];
    hostnameDetails: any;
    atkType: any;
    iocDetails: any;
    domainContacts: any = [];
    ipDetails: any;
    hashDetails: any;
    hash: any;
    analysisResult: any = [];
    analysisStat: any = [];
    resourceType: any = [];
    loading = false;
    emailDomain: any;
    malwareFamily: any;
    downloadMalwareLoader = false;
    malware = false;
    nomalwareiuploaded: any;
    communityTags: any = [];
    message: any;
    cveAccessVector: any;
    cveIntegrityImpact: any;
    cveScore: any;
    yaraAuthor: any;
    categoryArray: any = [];
    iocType: any;
    iocValue: any;
    items: MenuItem[];
    @Input() childMessage;
    submitSearch = false;
    initialized = false;
    partial = false;
    constructor(private http: HttpClient, private dashboardService: TipService, private spinner: NgxSpinnerService,
                private toastr: ToastrService, protected router: Router, private route: ActivatedRoute, public config: DynamicDialogConfig) {
    }

    getIocs() {
        this.route.queryParams.subscribe(params => {
            if (params.type && params.value) {
                this.iocType = params.type;
                this.iocValue = params.value;
            }
            if (params.submitSearch) {
                this.submitSearch = params.submitSearch;
            }

        });
        if (!this.iocType && !this.iocValue) {
            this.iocType = this.config.data.type;
            this.iocValue = this.config.data.value;
            this.partial = true;
        } else if (this.childMessage) {
            this.iocType = this.childMessage.type;
            this.iocValue = this.childMessage.value;
            this.submitSearch = this.childMessage.submitSearch;
        }
    }

    ngOnChanges(): void {
        if (this.initialized) {
            this.ngOnInit();

        }
    }

    ngOnInit() {
        this.initialized = true;
        this.getIocs();
        this.loader = true;
        this.items = [
            {label: 'Download PDF Report', icon: 'pi pi-download', command: () => {
                    this.downloadAttack();
                }},
            {separator: true},
            {label: 'Download STIX', icon: 'pi pi-download', command: () => {
                    this.downloadStixAttack(this.iocValue);
                }}
        ];
        this.spinner.show('ioc-spinner');
        this.onSubmit('false');
    }
    onSubmit(value) {
        this.loader = true;
        this.spinner.show('ioc-spinner');
        this.malware = false;
        this.iocDetails = false;
        this.ifnoData = false;
        this.domainContacts = [];
        this.categoryArray = [];
        this.communityTags = [];
        if (!value) {
            value = false;
        }
        if (this.iocType === 'Domain' || this.iocType === 'Email') {
            const indx = this.iocType.indexOf('@');
            this.emailDomain = this.iocType.substring(indx + 1);
        }
        // IPv4 URL Domain FileHash-MD5 FileHash-
        // SHA1 FileHash-SHA256 Malware YARA Email CVE Hostname
        if (this.iocType !== 'malware') {
            this.nomalwareiuploaded = false;
            this.dashboardService.getAptData(this.iocValue, this.iocType)
                .subscribe(
                    res => {
                        this.aptData = res;
                    },
                    err => {
                        this.showErrMessage(false, false, true, err);
                    });
            const historyWeek = null;
            this.dashboardService.searchComponent(this.iocValue, this.iocType, value, false, false, this.submitSearch).subscribe(
                res => {
                    this.searchAttack = res;
                    this.loader = false;
                    this.spinner.hide('ioc-spinner');
                    this.atkType = this.iocType;
                    this.datareport = this.searchAttack;
                    this.malware = false;
                    this.emailDetails = false;
                    this.cveDetails = false;
                    this.hostnameDetails = false;
                    this.yaraDetails = false;
                    this.export_loading = false;
                    if (this.searchAttack) {
                        if (this.searchAttack.factor) {
                            if ((!this.searchAttack.factor.threatFeed || this.searchAttack.factor.threatFeed && this.searchAttack.factor.threatFeed === 'NA')
                                && (!this.searchAttack.factor.registrationRecentlyUpdated || this.searchAttack.factor.registrationRecentlyUpdated && this.searchAttack.factor.registrationRecentlyUpdated === 'NA') &&
                                (!this.searchAttack.factor.recentlyRegistered || this.searchAttack.factor.recentlyRegistered && this.searchAttack.factor.recentlyRegistered === 'NA') &&
                                (!this.searchAttack.factor.domainIpResolution || this.searchAttack.factor.domainIpResolution && this.searchAttack.factor.domainIpResolution === 'NA') &&
                                (!this.searchAttack.factor.domainIpRegistrationCountry || this.searchAttack.factor.domainIpRegistrationCountry && this.searchAttack.factor.domainIpRegistrationCountry === 'NA') &&
                                (!this.searchAttack.factor.urlContainsIp || this.searchAttack.factor.urlContainsIp && this.searchAttack.factor.urlContainsIp === 'NA') &&
                                (!this.searchAttack.factor.urlNonStandardPort || this.searchAttack.factor.urlNonStandardPort && this.searchAttack.factor.urlNonStandardPort === 'NA') &&
                                (!this.searchAttack.routable || (!this.searchAttack.factor.ptrRecord || this.searchAttack.factor.ptrRecord && this.searchAttack.factor.ptrRecord === 'NA')) &&
                                (!this.searchAttack.factor.sslExpired || this.searchAttack.factor.sslExpired && this.searchAttack.factor.sslExpired === 'NA') &&
                                (!this.searchAttack.factor.sslSelfSigned || this.searchAttack.factor.sslSelfSigned && this.searchAttack.factor.sslSelfSigned === 'NA') &&
                                (!this.searchAttack.factor.sslSuspicious || this.searchAttack.factor.sslSuspicious && this.searchAttack.factor.sslSuspicious === 'NA') &&
                                (!this.searchAttack.factor.top10K || this.searchAttack.factor.top10K && this.searchAttack.factor.top10K === 'NA') &&
                                (!this.searchAttack.factor.ptrRecord || this.searchAttack.factor.ptrRecord && this.searchAttack.factor.ptrRecord === 'NA') &&
                                (!this.searchAttack.factor.top100Domain || this.searchAttack.factor.top100Domain && this.searchAttack.factor.top100Domain === 'NA') &&
                                (!this.searchAttack.factor.top1KDomain || this.searchAttack.factor.top1KDomain && this.searchAttack.factor.top1KDomain === 'NA') &&
                                (!this.searchAttack.factor.hostedAndRegisteredDifferentCountry || this.searchAttack.factor.hostedAndRegisteredDifferentCountry && this.searchAttack.factor.hostedAndRegisteredDifferentCountry === 'NA') &&
                                (!this.searchAttack.factor.foundInMultipleThreatFeeds || this.searchAttack.factor.foundInMultipleThreatFeeds && this.searchAttack.factor.foundInMultipleThreatFeeds === 'NA') &&
                                (!this.searchAttack.factor.foundInCdpFeeds || this.searchAttack.factor.foundInCdpFeeds && this.searchAttack.factor.foundInCdpFeeds === 'NA') &&
                                (!this.searchAttack.factor.sinkHoled || this.searchAttack.factor.sinkHoled && this.searchAttack.factor.sinkHoled === 'NA') &&
                                (!this.searchAttack.factor.allLinkedIpsReturnPtrRecord || this.searchAttack.factor.allLinkedIpsReturnPtrRecord && this.searchAttack.factor.allLinkedIpsReturnPtrRecord === 'NA') &&
                                (!this.searchAttack.factor.top100K || this.searchAttack.factor.top100K && this.searchAttack.factor.top100K === 'NA')) {
                                this.showFactorTab = false;
                            } else {
                                this.showFactorTab = true;
                            }
                        } else {
                            this.showFactorTab = false;

                        }
                        if (this.partial) {
                            this.searchAttack.partial = false;
                        }
                        if (this.searchAttack.portScans && this.searchAttack.portScans.length > 0) {
                            this.searchAttack.portScans.sort((portScanObj1, portScanObj2) => portScanObj1.state < portScanObj2.state ? 1 : -1);
                        }
                        if (this.iocType === 'IPv4' || this.iocType === 'URL'
                                || this.iocType === 'Domain' || this.iocType === 'FileHash-MD5' || this.iocType === 'FileHash-SHA1' ||
                                this.iocType === 'FileHash-SHA256') {
                                this.atkType = this.iocType;
                                this.iocDetails = true;
                                this.malware = false;
                                if (this.searchAttack.whoisRecord && this.searchAttack.whoisRecord.status && this.searchAttack.whoisRecord.status !== null) {
                                    const name = this.searchAttack.whoisRecord.status;
                                    const nameArr = name.split(',');
                                    this.searchAttack.whoisRecord.status = nameArr;
                                }
                                if (this.searchAttack.dnsList && this.searchAttack.dnsList.length > 0) {
                                    this.searchAttack.dnsList = this.searchAttack.dnsList.filter(({type}) => type !== 'RRSIG');
                                    this.searchAttack.dnsList = this.searchAttack.dnsList.filter(({type}) => type !== 'NSEC');
                                    this.searchAttack.dnsList = this.searchAttack.dnsList.filter(({type}) => type !== 'DNSKEY');
                                    for (const arr of this.searchAttack.dnsList) {
                                        arr.value = arr.value.replace(/(^[&\/\\#,+()@$!~^%.'":*?<>{}])/mg, '');
                                        arr.value = arr.value.replace(/([&\/\\#,+()@$!~^%.'":*?<>{}]$)/mg, '');
                                    }
                                }
                                this.analysisResult = [];
                                this.analysisStat = [];
                                this.resourceType = [];
                                if (this.searchAttack.avDetections) {
                                    if (this.searchAttack.avDetections.analysisResult) {
                                        this.analysisResult = Object.values(this.searchAttack.avDetections.analysisResult);
                                    }
                                    if (this.searchAttack.avDetections.analysisStat) {
                                        const key = Object.keys(this.searchAttack.avDetections.analysisStat);
                                        const value = Object.values(this.searchAttack.avDetections.analysisStat);
                                        for (let avDetectionsIndex = 0; avDetectionsIndex < key.length; avDetectionsIndex++) {
                                            const stats = {
                                                category: key[avDetectionsIndex],
                                                count: value[avDetectionsIndex]
                                            };
                                            this.analysisStat.push(stats);
                                        }
                                    }
                                    if (this.searchAttack.avDetections.peInfo) {
                                        if (this.searchAttack.avDetections.peInfo.resourceTypes) {
                                            const key = Object.keys(this.searchAttack.avDetections.peInfo.resourceTypes);
                                            const value = Object.values(this.searchAttack.avDetections.peInfo.resourceTypes);
                                            for (let avDetectionsIndex = 0; avDetectionsIndex < key.length; avDetectionsIndex++) {
                                                const stats = {
                                                    type: key[avDetectionsIndex],
                                                    count: value[avDetectionsIndex]
                                                };
                                                this.resourceType.push(stats);
                                            }
                                        }
                                    }
                                }
                                if (this.searchAttack.xforceIPThreatFeed) {
                                    if (this.searchAttack.xforceIPThreatFeed.history && this.searchAttack.xforceIPThreatFeed.history.length > 0) {
                                        this.categoryArray = [];
                                        for (const avDetectionsIndex of this.searchAttack.xforceIPThreatFeed.history) {
                                            const arr: any = [];
                                            if (avDetectionsIndex.cats) {
                                                const category = avDetectionsIndex.cats;
                                                const propertyNames = Object.keys(category);
                                                const propertyValues = Object.values(avDetectionsIndex.categoryDescriptions);
                                                const length = propertyNames.length;
                                                for (let avDetectionsIndex = 0; avDetectionsIndex < length; avDetectionsIndex++) {
                                                    const finalValue = {
                                                        name: propertyNames[avDetectionsIndex],
                                                    };
                                                    arr.push(finalValue);
                                                }
                                            }
                                            const history = {
                                                ip: avDetectionsIndex.ip,
                                                created: avDetectionsIndex.created,
                                                reason: avDetectionsIndex.reason,
                                                reasonDescription: avDetectionsIndex.reasonDescription,
                                                cats: arr
                                            };
                                            this.categoryArray.push(history);
                                        }
                                    }
                                }

                            }
                        if (this.iocType === 'YARA' && this.searchAttack) {
                                this.isellect = true;
                                this.iocDetails = false;
                                this.malware = false;
                                this.emailDetails = false;
                                this.cveDetails = false;
                                this.hostnameDetails = false;
                                this.yaraDetails = res;
                                const yaraRule = this.yaraDetails.content;
                                const yaraAuthor = yaraRule.indexOf('author');
                                const authorSubStr = yaraRule.substr(yaraAuthor, 6);
                                const yaraMeta = yaraRule.indexOf('meta%3A%20description');
                                const yar = yaraRule.indexOf('meta:');
                                const metaIndex = yaraRule.indexOf('meta%3A%20description%20%3D%20%22');
                                const dt = yaraRule.stringify;
                                const yaraRegix = yaraRule.replace(/[&\/\\#,+()~%.'*?<>]/g, '');
                                const met = yaraRegix.indexOf('meta description=');
                                this.yaraAuthor = yaraRegix;
                                const yaraSubStr = yaraRule.slice(yaraMeta + 4, yaraAuthor);
                                const yaraSliceAuthor = yaraRule.indexOf('author = ');
                                const yaraSlicSubStr = yaraRule.slice(yaraSliceAuthor + 8);
                                const yaraSliceMoreAuthor = yaraSlicSubStr.indexOf('strings');
                                const finalAuthor = yaraSlicSubStr.slice(0, yaraSliceMoreAuthor);
                            }
                        if (this.iocType === 'Email' && this.searchAttack) {
                                this.isellect = true;
                                this.iocDetails = false;
                                this.malware = false;
                                this.yaraDetails = false;
                                this.cveDetails = false;
                                this.hostnameDetails = false;
                                this.emailDetails = res;
                            }
                        if (this.iocType === 'CVE' && this.searchAttack) {
                                this.isellect = true;
                                this.iocDetails = false;
                                this.malware = false;
                                this.yaraDetails = false;
                                this.emailDetails = false;
                                this.hostnameDetails = false;
                                this.cveDetails = res;
                                if (this.cveDetails.general && this.cveDetails.general.cvss && this.cveDetails.general.cvss !== null) {
                                    this.cveAccessVector = this.cveDetails.general.cvss['Access-Vector'];
                                    this.cveIntegrityImpact = this.cveDetails.general.cvss['Integrity-Impact'];
                                    this.cveScore = this.cveDetails.general.cvss.Score;
                                }
                                if (this.cveDetails.general && this.cveDetails.general.exploits && this.cveDetails.general.exploits !== null) {
                                    for (let avDetectionsIndex = 0; avDetectionsIndex < this.cveDetails.general.exploits.length; avDetectionsIndex++) {
                                        this.cvedetection.push(this.cveDetails.general.exploits[avDetectionsIndex]);
                                    }
                                }
                            }
                        if (this.iocType === 'Hostname' && this.searchAttack) {
                                this.isellect = true;
                                this.iocDetails = false;
                                this.malware = false;
                                this.emailDetails = false;
                                this.yaraDetails = false;
                                this.cveDetails = false;
                                this.hostnameDetails = this.searchAttack;
                            }
                    } else {
                        this.loader = false;
                        this.spinner.hide('ioc-spinner');
                        this.ifnoData = true;
                    }
                },
                err => {
                    this.showErrMessage(false, false, true, err);
                }
            );
        }
        else if (this.iocType === 'Malware' || this.iocType === 'malware') {
            this.filesToUpload = this.dashboardService.saveMalware();
            this.malware = true;
            this.iocDetails = false;
            this.dashboardService.cuckooMalwareList().subscribe(
                res => {
                    this.loader = false;
                    this.spinner.hide('ioc-spinner');
                    this.malwareFamily = res;
                    if (this.malwareFamily) {
                        this.ifnoData = false;
                        this.nomalwareiuploaded = false;
                    } else {
                        this.ifnoData = true;
                        this.nomalwareiuploaded = false;
                        this.malware = false;
                        this.iocDetails = false;
                    }
                },
                err => {
                    this.showErrMessage(false, false, true, err);
                }
            );

            this.subscribenmalware = setInterval(() => {
                this.uploadFile(this.filesToUpload);
            }, 30000);
        }
    }

    public uploadFile(files: Set<File>): any {
        this.malware = true;
        this.iocDetails = false;
        this.ipDetails = false;
        this.hashDetails = false;
        this.yaraDetails = false;
        this.emailDetails = false;
        this.cveDetails = false;
        this.hostnameDetails = false;
        this.dashboardService.cuckooMalwareList().subscribe(
            res => {
                this.loader = false;
                this.spinner.hide('ioc-spinner');
                this.malwareFamily = res;
                if (this.malwareFamily) {
                    this.ifnoData = false;
                    this.nomalwareiuploaded = false;
                } else {
                    this.ifnoData = true;
                    this.nomalwareiuploaded = false;
                    this.ipDetails = false;
                    this.hashDetails = false;
                    this.malware = false;
                    this.iocDetails = false;
                    this.yaraDetails = false;
                    this.emailDetails = false;
                    this.cveDetails = false;
                    this.hostnameDetails = false;
                }
            }, err => {
                this.showErrMessage(false, false, true, err);
            }
        );

    }

    downloadAttack() {
        this.export_reporting = true;
        this.export_reporting_stix = false;
        this.reporttype = 'application/pdf';
        this.downloadType = this.iocType + ' Report.pdf';
        this.dashboardService.searchReporting(this.datareport, this.iocType)
            .subscribe(
                res => {
                    this.response = res;
                    const file = new Blob([this.response], {
                        type: this.reporttype,
                    });
                    const fileURL = window.URL.createObjectURL(file);
                    const link = document.createElement('a');
                    link.href = fileURL;
                    link.download = this.downloadType;
                    document.body.appendChild(link);
                    link.click();
                    this.toastr.success('Report Download Successful');
                    this.content = 'Report has been Downloaded';
                    this.export_reporting = false;
                    this.export_reporting_stix = false;
                },
                err => {
                    this.errors = err.status;
                    this.errors = err.status;
                    if (this.errors === 400) {
                        this.toastr.error('400 Bad Request ' + err.error.message);
                    } else {
                        this.toastr.error('Some Error Occurred Please try again');
                    }
                    this.export_reporting = false;
                    this.export_reporting_stix = false;
                });
    }

    downloadStixAttack(id) {
        this.export_reporting = false;
        this.export_reporting_stix = true;
        this.reporttype = 'application/json';
        this.downloadType = this.iocType + 'Report.json';
        this.dashboardService.searchStixReporting(id, this.iocType)
            .subscribe(
                res => {
                    this.response = res;
                    if (res && res !== null) {
                        const file = new Blob([this.response], {
                            type: this.reporttype,
                        });
                        const fileURL = window.URL.createObjectURL(file);
                        const link = document.createElement('a');
                        link.href = fileURL;
                        link.download = this.downloadType;
                        document.body.appendChild(link);

                        link.click();
                        this.toastr.success('Report Download Successful');
                    } else {
                        this.toastr.error('No record to Download');
                    }
                    this.export_reporting = false;
                    this.export_reporting_stix = false;
                },
                err => {
                    this.errors = err.status;
                    if (this.errors === 400) {
                        this.toastr.error('400 Bad Request ' + err.error.message);
                    } else if (this.errors === 404) {
                        this.toastr.error('404 Not Found');
                    } else if (this.errors === 409) {
                        this.toastr.error('Conflicted Data');
                    } else if (this.errors === 500) {
                        this.toastr.error('500 Internal Error Server');
                    }
                    this.export_reporting = false;
                    this.export_reporting_stix = false;
                });
    }

    downloadMalware(cuckooId) {
        this.downloadMalwareLoader = true;
        this.reporttype = 'application/x-gzip';
        this.downloadType = this.iocValue + 'Malware-Report.tar.gz';
        this.dashboardService.cuckooDownloadMalware(cuckooId)
            .subscribe(
                res => {
                    this.downloadMalwareLoader = false;
                    this.response = res;
                    if (res) {
                        const file = new Blob([this.response], {
                            type: this.reporttype,
                        });
                        const fileURL = window.URL.createObjectURL(file);
                        const link = document.createElement('a');
                        link.href = fileURL;
                        link.download = this.downloadType;
                        document.body.appendChild(link);

                        link.click();
                        this.toastr.success('Download Successful');
                        this.content = 'Malware Report has been Downloaded';
                    } else {
                        this.toastr.error('No record to Download');
                    }
                    this.export_reporting = false;
                    this.export_reporting_stix = false;
                },
                err => {
                    this.downloadMalwareLoader = false;
                    this.showErrMessage(false, false, false, err);
                    this.export_reporting = false;
                    this.export_reporting_stix = false;
                });
    }

    refresh() {
        this.onSubmit('true');
    }
    submitIndicatorSearch() {
        this.submitSearch = true;
        this.onSubmit('true');
    }

    viewAttackCampaign(id) {
        const url  = this.router.createUrlTree(['apt/viewCampaign'], {queryParams: {id, send: false}});
        window.open(url.toString(), '_blank');
    }

    viewGroups(id){
        const url  = this.router.createUrlTree(['apt/viewGroup'], {queryParams: {id, send: false}});
        window.open( url.toString(), '_blank');
    }

    showErrMessage(boolMal, boolIoc, boolData, err) {
        this.spinner.hide('ioc-spinner');
        this.malware = boolMal;
        this.iocDetails = boolIoc;
        this.ifnoData = boolData;
        this.toastr.error(err.error.message);
    }
}

