import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IocDetailsComponent } from './ioc-details.component';

describe('IocDetailsComponent', () => {
  let component: IocDetailsComponent;
  let fixture: ComponentFixture<IocDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IocDetailsComponent]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IocDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
