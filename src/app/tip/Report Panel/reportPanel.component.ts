import { Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {FormControl} from '@angular/forms';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {ReportModalComponent} from './report-modal/report-modal.component';
import {TipService} from '../../core-services/tip.service';
import {CalendarSettingService} from '../../core-services/calendarSetting.service';
@Component({
  selector: 'app-reportPanel',
  templateUrl: './reportPanel.component.html',
  styleUrls: ['./reportPanel.component.scss'],
  providers: [DialogService]
})

export class ReportPanelComponent implements OnInit {
  disableSelect = new FormControl(false);
  disableSector = new FormControl(false);
  ref: DynamicDialogRef;

  from: any;
  to: any;
  locale: any;
  ranges: any;
  selected: any;
  malwareac: any;
  ip: any;
  domain: any;


  dataSubmitted: any;
  selectedCategory: any;
  countryList: any = [];
  sectorList: any = [];
  selectedCountry: any = [];
  selectedSector: any = [];
  selectedCountryList: any = [];
  selectedSectorList: any = [];
  errors: any;
  response: any;
  atasel = true;
  sensel = true;
  malwareAll: any;
  ipAll: any;
  domainAll: any;
  json: any;
  content = '';
  ipdis: any = false;
  malwaredis: any = false;
  domaindis: any = false;

  generalReportBool = true;
  general = false;
  reportcond = false;
  popup = false;
  constructor(private tipService: TipService, private spinner: NgxSpinnerService,
              public dialogService: DialogService, private router: Router,
              private toastr: ToastrService, private caledarSettings: CalendarSettingService) { }

  ngOnInit(): void {
    this.generalReportBool = true;
    this.general = false;
    this.reportcond = false;

    this.locale = this.caledarSettings.locale;
    this.ranges = this.caledarSettings.ranges;
    this.selected = this.caledarSettings.reportselected;
    this.from  = this.caledarSettings.transformDate(this.selected.startDate);
    this.to  = this.caledarSettings.transformDate(this.selected.endDate);
    this.malwareac = [
      {name: 'Indicator Total Count', value: 'indicatorTotalCount-FileHash', selected: false}
    ];
    this.ip = [
      {name: 'Indicator Total Count', value: 'indicatorTotalCount-IPv4', selected: false},
      {name: 'Top Continent', value: 'topContinent-IPv4', selected: false},
      {name: 'Top Country', value: 'topCountry-IPv4', selected: false},
      {name: 'Top City', value: 'topCity-IPv4', selected: false},
      {name: 'City Maps IP', value: 'cityMapsIp-IPv4', selected: false},
    ];
    this.domain = [
      {name: 'Indicator Total Count', value: 'indicatorTotalCount-domain', selected: false},
      {name: 'Top Continent', value: 'topContinent-domain', selected: false},
      {name: 'Top Country', value: 'topCountry-domain', selected: false},
      {name: 'Top City', value: 'topCity-domain', selected: false},
      {name: 'All City Indicator', value: 'allCityIndicator-domain', selected: false},
    ];
    this.getCountryList();
    this.getSectorList();
  }
  selectDate() {
    this.from  = this.caledarSettings.transformDate(this.selected.startDate);
    this.to  = this.caledarSettings.transformDate(this.selected.endDate);
  }
  getCountryList() {
    this.tipService.getCountryList().subscribe(
        res => {
          if (res !== null) {
            this.countryList = res;
          } else { // status code: 204
          }
        }, err => {
        });
  }

  getSectorList() {
    this.tipService.getSectorList().subscribe(
        res => {
          if (res !== null) {
            this.sectorList = res;
          } else { // status code: 204
          }
        }, err => {
        });
  }

  getSelectedCountry(event) {

    if (event.value.length > 0) {
      this.generalReportBool = false;

      this.malwareAll = false;
      this.domainAll = false;
      this.ipAll = false;
      this.malwaredis = true;
      this.domaindis = true;
      this.ipdis = true;
    } else {
      this.generalReportBool = true;

      this.malwareAll = false;
      this.domainAll = false;
      this.ipAll = false;
      this.malwaredis = false;
      this.domaindis = false;
      this.ipdis = false;
    }
    this.selectedCountry = [];
    for (let i = 0; i < event.value.length; i++) {
      this.selectedCountry.push(event.value[i]);
    }
    this.selectedSector = [];
    this.selectedCountryList = [];
    this.selectedSectorList = [];
  }

  getSelectedSector(event) {
    if (event.value.length > 0) {
      this.generalReportBool = false;

      this.malwareAll = false;
      this.domainAll = false;
      this.ipAll = false;
      this.malwaredis = true;
      this.domaindis = true;
      this.ipdis = true;
    } else {
      this.generalReportBool = true;
      this.malwareAll = false;
      this.domainAll = false;
      this.ipAll = false;
      this.malwaredis = false;
      this.domaindis = false;
      this.ipdis = false;
    }
    this.selectedSector = [];
    for (let i = 0; i < event.value.length; i++) {
      this.selectedSector.push(event.value[i]);
    }
    this.selectedCountry = [];
    this.selectedCountryList = [];
    this.selectedSectorList = [];
  }

  malwareAttack(malware) {
    this.generalReportBool = true;
    if (this.malwareAll === true) {
      for (let i = 0; i < this.malwareac.length; i++) {
        this.malwareac[i].selected = this.malwareAll;
      }
      this.general = true;
      this.ipdis = true;
      this.domaindis = true;
      this.ipAll = false;
      this.domainAll = false;
    } else {
      this.general = false;

      for (let i = 0; i < this.malwareac.length; i++) {
        this.malwareac[i].selected = false;
      }
      this.ipdis = false;
      this.domaindis = false;
      this.ipAll = false;
      this.domainAll = false;
    }
  }

  ipAttack() {
    this.generalReportBool = true;

    if (this.ipAll === true) {
      for (let i = 0; i < this.ip.length; i++) {
        this.ip[i].selected = this.ipAll;
      }
      this.general = true;

      this.malwaredis = true;
      this.domaindis = true;
      this.malwareAll = false;
      this.domainAll = false;
    } else {
      for (let i = 0; i < this.ip.length; i++) {
        this.ip[i].selected = false;
      }
      this.general = false;

      this.malwaredis = false;
      this.domaindis = false;
      this.malwareAll = false;
      this.domainAll = false;
    }

  }

  domainAttack() {
    this.generalReportBool = true;

    if (this.domainAll === true) {
      for (let i = 0; i < this.domain.length; i++) {
        this.domain[i].selected = this.domainAll;

      }
      this.general = true;

      this.ipdis = true;
      this.malwaredis = true;
      this.ipAll = false;
      this.malwareAll = false;
    } else {
      for (let i = 0; i < this.domain.length; i++) {
        this.domain[i].selected = false;

      }
      this.general = false;

      this.ipdis = false;
      this.malwaredis = false;
      this.ipAll = false;
      this.malwareAll = false;
    }

  }

  malwaresingle(slect) {
    this.generalReportBool = true;

    if (this.malwareac[0].selected === false) {
      this.ipdis = false;
      this.domaindis = false;
      this.ipAll = false;
      this.domainAll = false;
      this.general = false;

    } else if (this.malwareac[0].selected === true) {
      this.ipdis = true;
      this.domaindis = true;
      this.ipAll = false;
      this.domainAll = false;
      this.general = true;

    }

  }

  ipssingle(slect) {
    this.generalReportBool = true;

    if (this.ip[0].selected === false && this.ip[1].selected === false &&
        this.ip[2].selected === false && this.ip[3].selected === false && this.ip[4].selected === false) {
      this.malwaredis = false;
      this.domaindis = false;
      this.malwareAll = false;
      this.domainAll = false;
      this.general = false;

    } else {
      this.malwaredis = true;
      this.domaindis = true;
      this.malwareAll = false;
      this.domainAll = false;
      this.general = true;

    }


  }

  domainsingle(slect) {
    this.generalReportBool = true;

    if (this.domain[0].selected === false && this.domain[1].selected === false &&
        this.domain[2].selected === false && this.domain[3].selected === false && this.domain[4].selected === false) {

      this.ipdis = false;
      this.malwaredis = false;
      this.ipAll = false;
      this.malwareAll = false;
      this.general = false;

    } else {
      this.ipdis = true;
      this.malwaredis = true;
      this.ipAll = false;
      this.malwareAll = false;
      this.general = true;

    }
  }

  onSubmit(event) {
    this.malwareAll = true;
    this.domainAll = true;
    this.ipAll = true;
    this.malwaredis = true;
    this.domaindis = true;
    this.ipdis = true;
    this.reportcond = true;
    // this.generalReportBool = false;
    this.general = true;

    let i = 0;
    for (const x in event) {
      if (event[x] === false) {
        i++;
      }
    }

    if (i === 11) {
      this.malwareAll = false;
      this.domainAll = false;
      this.ipAll = false;
      this.reportcond = false;
      this.malwaredis = false;
      this.domaindis = false;
      this.ipdis = false;
      // this.generalReportBool = false;
      this.general = false;

      this.toastr.error('Please select atleast one IoC type to download the report.');
    } else {
      this.spinner.show('report-spinner');

      this.tipService.iocReport(event, this.from, this.to)
          .subscribe(
              res => {
                this.spinner.hide('report-spinner');

                if (res || res !== null) {
                  this.response = res;
                  const downloadType = 'Attack Report.pdf';
                  const file = new Blob([this.response], {
                    type: 'application/pdf',
                  });
                  const fileURL = window.URL.createObjectURL(file);
                  const link = document.createElement('a');
                  link.href = fileURL;
                  link.download = downloadType;
                  document.body.appendChild(link);
                  link.click();
                  this.malwareAll = false;
                  this.domainAll = false;
                  this.ipAll = false;
                  this.reportcond = false;
                  this.malwaredis = false;
                  this.domaindis = false;
                  this.ipdis = false;
                  // this.generalReportBool = false;
                  this.general = false;

                  for (let i = 0; i < this.domain.length; i++) {
                    this.domain[i].selected = false;

                  }
                  for (let i = 0; i < this.ip.length; i++) {
                    this.ip[i].selected = false;
                  }
                  for (let i = 0; i < this.malwareac.length; i++) {
                    this.malwareac[i].selected = false;
                  }
                  this.toastr.success('Report Download Successful');

                } else {
                  this.toastr.error('No record to Download');
                  this.malwareAll = false;
                  this.domainAll = false;
                  this.ipAll = false;
                  this.reportcond = false;
                  this.malwaredis = false;
                  this.domaindis = false;
                  this.ipdis = false;
                  // this.generalReportBool = false;
                  this.general = false;

                }
              },
              error => {
                this.spinner.hide('report-spinner');
                this.malwareAll = false;
                this.domainAll = false;
                this.ipAll = false;
                this.reportcond = false;
                this.malwaredis = false;
                this.domaindis = false;
                this.ipdis = false;
                // this.generalReportBool = false;
                this.general = false;

                this.toastr.error('Please select any one type to Schedule/download ');
              });
    }


  }
  onSubmitCSV(event) {
    this.malwaredis = true;
    this.domaindis = true;
    this.ipdis = true;
    this.reportcond = true;
    // this.generalReportBool = false;
    this.general = true;
    if ((this.malwareAll === false || this.malwareAll === null || this.malwareAll === undefined)
        && (this.ipAll === false || this.ipAll === null || this.ipAll === undefined)
        && (this.domainAll === false || this.domainAll === null || this.domainAll === undefined)) {
      this.malwareAll = false;
      this.domainAll = false;
      this.ipAll = false;
      this.reportcond = false;
      this.malwaredis = false;
      this.domaindis = false;
      this.ipdis = false;
      // this.generalReportBool = false;
      this.general = false;
      this.toastr.error('Please select atleast one IoC type to download the report.');
    } else {
      let type;
      if (this.malwareAll === true) {
        type = 'FileHash';
      } else if (this.domainAll === true) {
        type = 'Domain';
      } if (this.ipAll === true) {
        type = 'IPv4';
      }
      this.spinner.show('report-spinner');

      this.tipService.iocReportCSV(type, this.from, this.to)
          .subscribe(
              res => {
                this.spinner.hide('report-spinner');
                if (res || res !== null) {
                  this.response = res;
                  const downloadType = 'Attack Report.csv';
                  const file = new Blob([this.response], {
                    type: 'application/csv',
                  });
                  const fileURL = window.URL.createObjectURL(file);
                  const link = document.createElement('a');
                  link.href = fileURL;
                  link.download = downloadType;
                  document.body.appendChild(link);
                  link.click();
                  this.malwareAll = false;
                  this.domainAll = false;
                  this.ipAll = false;
                  this.reportcond = false;
                  this.malwaredis = false;
                  this.domaindis = false;
                  this.ipdis = false;
                  // this.generalReportBool = false;
                  this.general = false;

                  for (let i = 0; i < this.domain.length; i++) {
                    this.domain[i].selected = false;

                  }
                  for (let i = 0; i < this.ip.length; i++) {
                    this.ip[i].selected = false;
                  }
                  for (let i = 0; i < this.malwareac.length; i++) {
                    this.malwareac[i].selected = false;
                  }
                  this.toastr.success('Report Download Successful');

                } else {
                  this.toastr.error('No record to Download');
                  this.malwareAll = false;
                  this.domainAll = false;
                  this.ipAll = false;
                  this.reportcond = false;
                  this.malwaredis = false;
                  this.domaindis = false;
                  this.ipdis = false;
                  // this.generalReportBool = false;
                  this.general = false;

                }
              },
              error => {
                this.spinner.hide('report-spinner');
                this.malwareAll = false;
                this.domainAll = false;
                this.ipAll = false;
                this.reportcond = false;
                this.malwaredis = false;
                this.domaindis = false;
                this.ipdis = false;
                // this.generalReportBool = false;
                this.general = false;

                this.toastr.error('Please select any one type to Schedule/download ');
              });
    }


  }
  downloadReport(type) {
    this.generalReportBool = true;
    this.malwareAll = true;
    this.domainAll = true;
    this.ipAll = true;
    this.malwaredis = true;
    this.domaindis = true;
    this.ipdis = true;
    this.reportcond = true;
    this.general = true;
    if ((this.selectedSector && this.selectedSector.length !== 0) || (this.selectedCountry && this.selectedCountry.length !== 0)) {
      if (this.selectedCountry && this.selectedCountry.length !== 0) {
        for (let i = 0; i < this.selectedCountry.length; i++) {
          const data = {
            countryName: this.selectedCountry[i]
          };
          this.selectedCountryList.push(data);
        }
        const country = {
          countries: this.selectedCountryList,
          from: this.from,
          reportType: type,
          to: this.to
        };
        this.dataSubmitted = country;
        this.selectedCategory = 'country';
      } else if (this.selectedSector && this.selectedSector.length !== 0) {
        for (let i = 0; i < this.selectedSector.length; i++) {
          const data = {
            sectorValue: this.selectedSector[i]
          };
          this.selectedSectorList.push(data);
        }
        const sector = {
          sector: this.selectedSectorList,
          from: this.from,
          reportType: type,
          to: this.to
        };
        this.dataSubmitted = sector;
        this.selectedCategory = 'sector';
      }
      this.spinner.show('report-spinner');
      this.tipService.postIOC(this.dataSubmitted, this.selectedCategory).subscribe(
              res => {
                this.generalReportBool = false;

                this.malwareAll = false;
                this.domainAll = false;
                this.ipAll = false;
                this.malwaredis = false;
                this.domaindis = false;
                this.ipdis = false;

                this.reportcond = false;
                this.general = false;
                if (res || res !== null) {
                  this.response = res;
                  const downloadType = 'Attack Report.' + type;
                  const file = new Blob([this.response], {
                    type: 'application/' + type,
                  });
                  const fileURL = window.URL.createObjectURL(file);
                  const link = document.createElement('a');
                  link.href = fileURL;
                  link.download = downloadType;
                  document.body.appendChild(link);
                  this.spinner.hide('report-spinner');

                  link.click();
                  this.toastr.success( 'Report Download Successful!');
                } else {
                  this.spinner.hide('report-spinner');
                  this.toastr.error('No record to Download');
                }
                this.dataSubmitted = null;
                this.selectedCountry = [];
                this.selectedSector = [];
                this.selectedCountryList = [];
                this.selectedSectorList = [];
              },
              err => {
                this.errors = err.status;
                this.spinner.hide('report-spinner');
                this.generalReportBool = false;

                this.malwareAll = false;
                this.domainAll = false;
                this.ipAll = false;
                this.malwaredis = false;
                this.domaindis = false;
                this.ipdis = false;

                this.reportcond = false;
                this.general = false;
                this.selectedCountryList = [];
                this.selectedSectorList = [];
                if (this.errors === 400) {
                  this.toastr.error( this.errors.error.message);
                } else if (this.errors === 404) {
                  this.toastr.error( '404 Not Found');
                } else if (this.errors === 409) {
                this.toastr.error( 'Conflicted Data');
                } else {
                  this.toastr.error(  '500 Internal Server Error');
                }
              });
    } else {
      this.generalReportBool = false;

      this.malwareAll = false;
      this.domainAll = false;
      this.ipAll = false;
      this.malwaredis = false;
      this.domaindis = false;
      this.ipdis = false;

      this.reportcond = false;
      this.general = false;
      this.toastr.error( 'Please select atleast one country/sector to download');

    }
  }

  SheduleReport(event) {
    event.from = this.from;
    event.to = this.to;
    let i = 0;
    for (const x in event) {
      if (event[x] !== false && (x === 'indicatorTotalCount-FileHash' || x === 'indicatorTotalCount-IPv4' ||
       x === 'topContinent-IPv4' || x === 'topCountry-IPv4' || x === 'topCity-IPv4' ||
          x === 'cityMapsIp-IPv4' || x === 'indicatorTotalCount-domain' || x === 'topContinent-domain'
          || x === 'topCountry-domain' || x === 'topCity-domain' || x === 'topCity-domain'
          || x === 'allCityIndicator-domain')) {
        i++;
      }
    }
    if (i > 0)  {
      this.ref = this.dialogService.open(ReportModalComponent, {
        header: 'Schedule Report',
        data: event,
        width: '70%',
        contentStyle: {'max-height': '500px', overflow: 'auto'},
        baseZIndex: 10000
      });
    } else {
      this.toastr.error( 'Kindly Select At least One Of the Categories Of Attack');

    }
  }
  shedulelist() {
    this.router.navigate(['tip/reportList']);
  }
}
