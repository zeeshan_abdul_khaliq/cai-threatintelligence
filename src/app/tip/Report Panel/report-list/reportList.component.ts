import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {ConfirmationService, MenuItem} from 'primeng/api';
import {DatePipe} from '@angular/common';
import {TipService} from '../../../core-services/tip.service';
import {CalendarSettingService} from '../../../core-services/calendarSetting.service';

@Component({
    selector: 'app-report-list',
    templateUrl: './reportList.component.html',
    styleUrls: ['./reportList.component.scss'],
    providers: [ConfirmationService]

})
export class ReportListComponent implements OnInit {
    p: any;
    usr;
    userrole: any;
    org: any;
    scheduledata: any;
    ifdata = true;
    ifnodata = false;
    content = '';
    items: MenuItem[];


    constructor(private reportService: TipService, private spinner: NgxSpinnerService,
                private toasterService: ToastrService, protected router: Router,
                private calendarS: CalendarSettingService,
                private confirmationService: ConfirmationService, private datefilter: DatePipe) {
        this.items = [
            {label: 'Dashboard', routerLink: '/tip/dashboard', routerLinkActiveOptions: true},
            {label: 'Report Panel', routerLink: '/tip/reports', routerLinkActiveOptions: true},
        ];
    }

    ngOnInit() {
        this.getReportListing();
        {

        }
    }

    getReportListing() {
        this.userrole = sessionStorage.getItem('role');
        this.org = sessionStorage.getItem('organization');
        this.usr = sessionStorage.getItem('UserFirstName');
        this.spinner.show('reportlist-spinner');
        this.reportService.getscheduleReport(this.usr)
            .subscribe(res => {
                this.spinner.hide('reportlist-spinner');
                this.scheduledata = res;
                if (this.scheduledata !== null) {
                    this.ifdata = true;
                    this.ifnodata = false;
                    for (let i = 0; i < this.scheduledata.length; i++) {
                        this.scheduledata[i].reportFrom = this.calendarS.transformRDate(this.scheduledata[i].reportFrom);
                        this.scheduledata[i].reportTo = this.calendarS.transformRDate(this.scheduledata[i].reportTo);
                        this.scheduledata[i].endRange = this.calendarS.transformRDate(this.scheduledata[i].endRange);
                        this.scheduledata[i].sendingTime = this.calendarS.transformRDate(this.scheduledata[i].sendingTime);
                    }
                } else {
                    this.ifdata = false;
                    this.ifnodata = true;
                }
                },
                err => {
                this.spinner.hide('reportlist-spinner');
                // console.log('sasa', err);
                });
    }

    Deletesch(event) {
        const id = event.sid;
        let thread = '';
        if (event.currentThread !== null) {
            thread = event.currentThread;
        }
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete the schedule report?',
            header: 'Confirmation',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {

                this.spinner.show('reportlist-spinner');
                this.reportService.deletescheduleReport(id, thread)
                    .subscribe(res => {
                        console.log(res);
                        this.content = `Scheduled Report Has Been Deleted Successfully!`;
                        this.toasterService.success(this.content);
                        this.getReportListing();
                    }, error => {
                        console.log('1', error);

                        if (error.status === 401) {
                            sessionStorage.clear();
                            localStorage.clear();
                            this.router.navigate(['/logout']);
                        } else
                        if (error.status === 200) {
                            this.content = `Scheduled Report Has Been Deleted Successfully!`;
                            this.toasterService.success(this.content);
                            this.getReportListing();

                        } else {
                            this.getReportListing();
                        }
                    });
            },
            reject: () => {
            }
        });

    }


}
