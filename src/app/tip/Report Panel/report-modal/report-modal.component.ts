import {Component, Input, OnInit} from '@angular/core';
import {DatePipe} from '@angular/common';
import {Router} from '@angular/router';
import {DialogService, DynamicDialogConfig, DynamicDialogRef} from 'primeng/dynamicdialog';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TipService} from '../../../core-services/tip.service';

@Component({
  selector: 'app-report-modal',
  templateUrl: './report-modal.component.html',
  styleUrls: ['./report-modal.component.scss'],
  providers: [DialogService]

})
export class ReportModalComponent implements OnInit {
  reportingOTPForm: FormGroup;
  reportingRRForm: FormGroup;
  user: any;
  org: any;
  tabtitle: any;
  sendingtime: any;
  enddate: any;
  too: any;
  fromm: any;
  emaillist: any;
  emaillistRR: any;
  eventdata: any;
  reptype: any;
  status: any;
  lastSeen: any;
  onetimesheduledate: any;
  onetimesheduletime: any;
  mindate: any;
  mintime: any;
  recurrencepattern: any = 1;
  selectedendradio: any = 'noend';
  multitimesheduledateendby: any;
  multitimesheduletimeafter: any;
  multitimesheduledateafter: any;
  mindateselected: boolean;
  minenddateselected: boolean;
  mintimetocheck: any;
  numberday: any;
  repeatdaily = [1, 2, 3, 4, 5, 6, 7];
  repeatweekly = [1, 2, 3, 4];
  repeatmonthly = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  timearray: number[];
  reportscheduleformat = [
    {name: 'Daily', value: 'Daily', selected: true},
    {name: 'Weekly', value: 'Weekly', selected: false},
    {name: 'Monthly', value: 'Monthly', selected: false},
    {name: 'Yearly', value: 'Yearly', selected: false},
  ];
  selectedfrequencystate: any;

  constructor( public ref: DynamicDialogRef, public config: DynamicDialogConfig, private reportService: TipService,
               private toastr: ToastrService, private datefilter: DatePipe, private formBuilder: FormBuilder,
               private spinner: NgxSpinnerService, protected router: Router) {
    this.emaillist = [];
    this.emaillistRR = [];
    this.eventdata = this.config.data;
    this.selectedfrequencystate = this.reportscheduleformat[0].value;
    this.timearray = [];
    const dt = new Date;
    const tm = new Date().getTime();
    this.mindate = this.datefilter.transform(dt, 'yyyy-MM-dd');
    this.mintime = this.datefilter.transform(tm, 'HH');
    this.mintime = parseInt(this.mintime, 10);
    this.mintime = this.mintime + 1;
    this.onetimesheduledate = this.mindate;
    this.onetimesheduletime = this.mintime;
    this.multitimesheduledateendby = this.mindate;
    this.multitimesheduletimeafter = this.mintime;
    this.multitimesheduledateafter = this.mindate;
    this.mintimetocheck = this.mintime;
    this.numberday = this.datefilter.transform(new Date(this.multitimesheduledateafter), 'd');
    this.reportingOTPForm = this.formBuilder.group({
      emaillist: ['', [Validators.required,
        Validators.pattern( '^[a-zA-Z0-9.!#$%&â€™*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$')]],
      onetimesheduledate: ['', [Validators.required]],
      onetimesheduletime: [this.onetimesheduletime],
    });
    this.reportingRRForm = this.formBuilder.group({
      emaillist: ['', [Validators.required, Validators.pattern( '^[a-zA-Z0-9.!#$%&â€™*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$')]],
      selectedfrequencystate: [this.selectedfrequencystate],
      recurrencepattern: [this.recurrencepattern],
      multitimesheduledateafter: [this.multitimesheduledateafter],
      multitimesheduletimeafter: [this.multitimesheduletimeafter],
      selectedendradio: [this.selectedendradio],
      multitimesheduledateendby: [this.multitimesheduledateendby],
    });
    this.tabtitle = 'One-Time Report Run';

  }

  changeval() {
    console.log(this.multitimesheduledateafter);
    this.numberday = this.datefilter.transform(new Date(this.multitimesheduledateafter), 'd');
  }

  closeModal() {
    this.ref.close();
  }

  changesr() {
    this.recurrencepattern = 1;
  }

  changetimevalue(event) {
    console.log(this.selectedendradio);
    if (this.selectedendradio === 'end' && this.multitimesheduledateendby < this.multitimesheduledateafter) {
      this.toastr.error('End date should be greater than Start date');
      this.minenddateselected = true;
    } else {
      this.timearray = [];
      this.minenddateselected = false;
      if (event.target.value > this.mindate) {
        this.mindateselected = false;
        for (let i = 1; i <= 24; i++) {
          this.timearray.push(i);
        }
      } else if (event.target.value === this.mindate) {
        this.mindateselected = false;
        if (this.tabtitle === 'One-Time Report Run') {
          this.onetimesheduletime = this.mintime;
        } else {
          this.multitimesheduletimeafter = this.mintime;
        }
        for (let i = this.mintime; i <= 24; i++) {
          this.timearray.push(i);
        }
        event.stopPropagation();
      } else if (event.target.value < this.mindate) {
        this.toastr.error('Start date should be greater than or equal to Current date.');
        this.mindateselected = true;
      }
    }
  }
  get errorControl() {
    return this.reportingOTPForm.controls;
  }
  get recrep() {
    return this.reportingRRForm.controls;
  }

  oneTimeReport() {
    console.log(this.reportingOTPForm.value);
    if (this.reportingOTPForm.invalid) {
      this.toastr.error('Please fill the One Time Report Form!');
      return;
    }
    const event = this.reportingOTPForm.value;
    event.recurrencepattern = '1';
    if (event.emaillist !== undefined) {
        event.selectedfrequencystate = 'oneTime';
        this.sendingtime = event.onetimesheduledate + 'T' + event.onetimesheduletime + ':00:00';
        const now = new Date();
        const hr = this.datefilter.transform(this.sendingtime, 'HH');
        let nmb = parseInt(hr, 10);
        nmb = nmb + 1;
        this.enddate = this.datefilter.transform(new Date(this.sendingtime).setHours(nmb), 'yyyy-MM-ddTHH:mm:ss');
        this.spinner.show('reporting-spinner');

        this.reportService.addscheduleReport(this.eventdata, event, this.eventdata.from, this.eventdata.to, this.reptype, this.user,
        this.enddate, this.sendingtime, this.org)
        .subscribe(
          res => {
            this.spinner.hide('reporting-spinner');
            this.ref.close();
            this.toastr.success('Report Has Been Scheduled!');
            this.router.navigate(['tip/reportList']);
          }, error => {
            this.spinner.hide('reporting-spinner');
            this.toastr.error('Please add the correct information');
            if (error.status === 401) {
              this.router.navigate(['/auth/logout']);
            } else {
              console.log(error);
            }
          });
    } else {
      this.toastr.error('Kindly Add Atleast One Email in First Input Field Named As Distribution List!');
    }
  }
  recurringReport() {
    console.log(this.reportingRRForm.value);
    if (this.reportingRRForm.invalid) {
      this.toastr.error('Please fill the Recurring Report Form!');
      return;
    }
    const event = this.reportingRRForm.value;
    if (event.emaillist !== undefined) {
        this.sendingtime = event.multitimesheduledateafter + 'T' + event.multitimesheduletimeafter + ':00:00';
        const now = new Date();
        if (event.selectedendradio === 'end') {
          console.log('Date is ', this.enddate, this.sendingtime);
          this.enddate = new Date((Date.parse(event.multitimesheduledateendby))).toISOString();
        } else {
         const dt = new Date();
         this.enddate = this.datefilter.transform(new Date(dt.setFullYear(dt.getFullYear() + 1)), 'yyyy-MM-ddTHH:mm:ss');

        }
        this.spinner.show('reporting-spinner');

        this.reportService.addscheduleReport(this.eventdata, event, this.eventdata.from, this.eventdata.to,
          this.reptype, this.user, this.enddate, this.sendingtime, this.org).subscribe(
          res => {
            this.spinner.hide('reporting-spinner');
            this.ref.close();
            this.toastr.success('Report Has Been Scheduled!');
            this.router.navigate(['tip/reportList']);
          }, error => {
            this.spinner.hide('reporting-spinner');
            this.toastr.error('Please add the correct information');
            if (error.status === 401) {
              this.router.navigate(['/auth/logout']);
            } else {
              console.log(error);
            }
          });
    } else {
      this.toastr.error('Kindly Add Atleast One Email in First Input Field Named As Distribution List!');
    }
  }

  ngOnInit() {
    this.mindateselected = false;
    this.minenddateselected = false;
    for (let i = this.mintime; i <= 24; i++) {
      this.timearray.push(i);
    }
    this.user = sessionStorage.getItem('UserFirstName');
    this.org = sessionStorage.getItem('organization');
  }

  onChange(event) {
    console.log(event);
    if (event.index === 0) {
      this.tabtitle = 'One-Time Report Run';

    } else {
      this.tabtitle = 'multiple';
    }
  }
  cancel() {
    this.ref.close();
  }
  add(event: any): void {
    console.log(event, event.value);
    const input = event.input;
    let value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      value = value.trim();
      if (this.emaillist.find((test) => test.toLowerCase() === value.toLowerCase()) === undefined) {
          this.emaillist.push(value.trim());
          this.reportingOTPForm.value.emaillist = this.emaillist;
      }

    }


    // Reset the input value
    if (input) {
      input.value = '';
    }
  }
  addRR(event: any): void {
    console.log(event, event.value);
    const input = event.input;
    let value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      value = value.trim();
      if (this.emaillistRR.find((test) => test.toLowerCase() === value.toLowerCase()) === undefined) {
          this.emaillistRR.push(value.trim());
          this.reportingRRForm.value.emaillist = this.emaillistRR;
      }

    }


    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(event: any): void {
    console.log(event.value);
    const index = this.emaillist.findIndex(x => x === event.value);

    if (index >= 0) {
      this.emaillist.splice(index, 1);
      this.reportingOTPForm.value.emaillist = this.emaillist;
    }
  }
  removeRR(event: any): void {
    console.log(event.value);
    const index = this.emaillistRR.findIndex(x => x === event.value);

    if (index >= 0) {
      this.emaillistRR.splice(index, 1);
      this.reportingRRForm.value.emaillist = this.emaillistRR;
    }
  }
  checkstarton(event) {
    if (this.multitimesheduledateendby < this.mindate) {
      this.toastr.error('Kindly Select Atleast One Of the Following Categories Of Attack');
      this.minenddateselected = true;
    } else if (this.multitimesheduledateendby < this.multitimesheduledateafter) {
      this.toastr.error('End Date should be greater than Start Date!');
      this.minenddateselected = true;
    } else {
      this.minenddateselected = false;
    }
  }
}
