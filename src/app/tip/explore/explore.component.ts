import { Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {map} from 'rxjs/operators';
import {Table} from 'primeng/table';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {GetExploreAllFeeds, HashIndicatorCounts, SummaryData} from '../../core-services/tip-interfaces';
import {TipService} from '../../core-services/tip.service';
import {IocDetailsComponent} from '../ioc-details/ioc-details.component';
@Component({
  selector: 'app-explore',
  templateUrl: './explore.component.html',
  styleUrls: ['./explore.component.scss'],
  providers: [DialogService]
})

export class ExploreComponent implements OnInit {
  @ViewChild('dt') exploreTable: Table | undefined;
  exploreData: SummaryData[];
  getexploreAllFeedsResponse: GetExploreAllFeeds;
  type: string;
  typehtml: string;
  filter$: Observable<any>;
  counter = 0;
  totalRecords: number;
  country: string;
  sectorType: string;
  error: any;
  hashType_md5: number;
  hashType_sha1: number;
  hashType_sha256: number;
  spinnerReport = false;
  reporttype: string;
  downloadType: string;
  hashCountsResponse: HashIndicatorCounts[];
  checkIfFilter = false;
  stixURL: any;
  searchDelveResponse: any;
  downloadPdfAttackResponse: any;
  constructor(private tipService: TipService, private toastr: ToastrService, private router: Router,
              public dialogService: DialogService,
              private spinner: NgxSpinnerService, private route: ActivatedRoute) {
    this.counter = 0;
    this.filter$ = this.route.queryParamMap.pipe(
        map((params: ParamMap) => params.get('type')),
    );
    this.filter$.subscribe(param => {
      this.checkIfFilter = false;
      this.counter = 0;
      this.type = param;
      this.typehtml = param;
      this.country = null;
      this.sectorType = null;
      const country = this.route.snapshot.queryParamMap.get('country');
      const sector = this.route.snapshot.queryParamMap.get('sector');
      if (country && country !== null && country !== undefined) {
        this.checkIfFilter = true;
        this.country = country;
      }
      if (sector && sector !== null && sector !== undefined) {
        this.sectorType = sector;
        this.checkIfFilter = true;
      }
      if (this.type === 'Hash') {
        this.type = 'FileHash-MD5';
        this.gethashcounts();
        this.getExplore();
      } else {
        this.getExplore();
      }
    });
  }
  ngOnInit(): void {
  }
  checkParam() {
    const val = this.type;
    if (val === 'Hostname' || val === 'YARA' || val === 'Email' || val === 'CVE') {
      return false;
    }  else {
      return true;
    }
  }
  getExplore() {
    this.spinner.show('explore-spinner');
    this.tipService.getexploreAllFeeds(this.type, this.counter, this.sectorType, this.country).subscribe(
        getexploreAllFeedsResponse => {
          this.spinner.hide('explore-spinner');
          this.getexploreAllFeedsResponse = getexploreAllFeedsResponse;
          if (this.getexploreAllFeedsResponse) {
            this.totalRecords = this.getexploreAllFeedsResponse.totalElements;
            this.exploreData = [...this.getexploreAllFeedsResponse.summaryData];
            if (this.counter === 0) {
              this.exploreTable.first = 0;

            }
          } else {
            this.totalRecords = 0;
            this.exploreData = [];
          }

        }, err =>
        {
          this.spinner.hide('explore-spinner');
          this.error = err;
          if (this.error === 400) {
            this.toastr.error( this.error.error.message);
          } else  {
            this.toastr.error('Oops Problem Occured Internal Server Error');
          }
        }
    );
  }
  gethashcounts() {
    this.tipService.getHashFeedsCount().subscribe(getHashFeedCountsResponse => {
      this.hashCountsResponse = getHashFeedCountsResponse;
      for (const hash of this.hashCountsResponse) {
        if (hash.indicatorType === 'FileHash-MD5') {
          this.hashType_md5 = hash.totalCount;
        } else if (hash.indicatorType === 'FileHash-SHA256') {
          this.hashType_sha256 = hash.totalCount;
        } else if (hash.indicatorType === 'FileHash-SHA1') {
          this.hashType_sha1 = hash.totalCount;
        }
      }
    }, err => {
      this.error = err;
      this.toastr.error(this.error.error.message);
    });
  }
  pagination(event) {
    this.counter  = event.first / 10;
    this.getExplore();
  }
  changetab(event) {
    this.counter = 0;
    if (event.index === 0) {
      this.type = 'FileHash-MD5';
    } else if (event.index === 1){
      this.type = 'FileHash-SHA1';
    } else if (event.index === 2) {
      this.type = 'FileHash-SHA256';
    }
    this.getExplore();
  }
  downloadPdfAttack(type, title) {
    this.reporttype = 'application/pdf';
    this.downloadType = type + ' Report.pdf';
    this.type = type;
    this.spinnerReport = true;
    this.tipService.searchDelve(title, type).subscribe(
        searchDelveResponse => {
          this.downloadPdfAttackResponse = searchDelveResponse;
          this.spinnerReport = false;
          this.stixURL = this.downloadPdfAttackResponse.resource;
          this.tipService.searchReporting(this.downloadPdfAttackResponse, type)
              .subscribe(
                  searchDelveResponse => {
                    this.searchDelveResponse = searchDelveResponse;
                    this.spinnerReport = false;
                    const file = new Blob([this.searchDelveResponse], {
                      type: this.reporttype,
                    });
                    const fileURL = window.URL.createObjectURL(file);
                    const link = document.createElement('a');
                    link.href = fileURL;
                    link.download = this.downloadType;
                    document.body.appendChild(link);
                    link.click();
                    this.toastr.success('Report Download Successful');
                  },
                  err => {
                    this.error = err.status;
                    this.spinnerReport = false;
                    if (this.error === 400) {
                      this.toastr.error( this.error.error.message);
                    } else  {
                      this.toastr.error('Oops Problem Occured Some Problem Occurred while ' +
                            'downloading reporting! Please Try later');
                    }
                  });
        },
        err =>  {
          this.error = err.status;
          this.spinnerReport = false;
          if (this.error === 400) {
            this.toastr.error( this.error.error.message);
          } else {
            this.toastr.error('Oops Problem Occured Some Problem Occurred while ' +
                'downloading reporting! Please Try later');
          }
        }
    );
  }

  downloadStixAttack(type, title) {
    this.spinnerReport = true;
    this.reporttype = 'application/json';
    this.downloadType = type + 'Report.json';
    this.tipService.searchStixReporting(title, type)
        .subscribe(
            stixReportingResponse => {
              this.searchDelveResponse = stixReportingResponse;
              this.spinnerReport = false;
              if (stixReportingResponse) {
                const file = new Blob([this.searchDelveResponse], {
                  type: this.reporttype,
                });
                const fileURL = window.URL.createObjectURL(file);
                const link = document.createElement('a');
                link.href = fileURL;
                link.download = this.downloadType;
                link.click();
                this.toastr.success('Report Download Successful');
              } else {
                this.toastr.error('No record to Download');
              }
            },
            err => {
              this.spinnerReport = false;
              this.error = err.status;
              if (this.error === 400) {
                this.toastr.error(this.error.error.message);
              } else if (this.error === 404) {
                this.toastr.error('404 Not Found');
              } else if (this.error === 409) {
                this.toastr.error('Conflicted Data');
              } else {
                this.toastr.error('Oops Problem Occurred Some Problem Occurred while downloading reporting! Please Try later');
              }
            });
  }

  openDialog(value) {
    this.dialogService.open(IocDetailsComponent, {
      header: 'Indicator Details',
      width: '90%',
      contentStyle: {'min-height': '800px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data:  {type: this.type, value}
    });
  }
  clearFilter() {
    this.country = null;
    this.sectorType = null;
    this.counter = 0;
    this.getExplore();
  }
}
