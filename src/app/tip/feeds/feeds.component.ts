import {Component, OnInit} from '@angular/core';
import {environment} from '../../../environments/environment';
import {Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {TipService} from '../../core-services/tip.service';

@Component({
  selector: 'app-feeds',
  templateUrl: './feeds.component.html',
  styleUrls: ['./feeds.component.css'],
  providers: [DatePipe]
})
export class FeedsComponent implements OnInit {
  users: any;
  username: any;
  o: any;
  name: any;
  email: any;
  emailId: any;
  role: any;
  displayName: any;
  createdBy: any;
  createdDate: any;
  error: any;
  reporttype: any;
  downloadType: any;
  response: any;
  errors: any;
  content = '';
  stixURL: any;
  hide = true;
  date: any;
  passwordValue: any = 'admin@TRIAM_TI';
  value: any;
  STIX_Url_Feeds: any;
  updatedDate: any;
  dateNew: any;
  constructor(private tipService: TipService, private  datePipe: DatePipe, private toastr: ToastrService,
              private spinner: NgxSpinnerService) {
    this.date = '2019-04-10T00:00:00Z';
    this.stixURL = environment.Stix_URL;
    this.STIX_Url_Feeds = environment.STIX_Url_Feeds;
    this.passwordValue = environment.Password;
  }

  changeDate(event) {
    const date = new  Date (event);
    const latest_date = this.datePipe.transform(date, 'yyyy-MM-dd');
    if (event) {
      this.updatedDate = '?added_after=' + latest_date;
      this.stixURL = environment.Stix_URL + this.updatedDate;
    } else {
      this.updatedDate = '';
      this.stixURL = environment.Stix_URL;
    }
  }

  onChange(event) {

    console.log('event', event);
    this.value = event;
  }

  onSave() {
    if (this.value) {
      this.toastr.success( 'Successfully set to ' + this.value);
    }
    this.value = '';
  }

  ngOnInit() {
  }

  authorization() {
    if (sessionStorage.getItem('role') === 'ROLE_SUPER_ADMIN' || sessionStorage.getItem('role') === 'ROLE_CLIENT_ADMIN' ||
      sessionStorage.getItem('role') === 'ROLE_SOC_ADMIN') {
      return true;
    } else {
      return false;
    }
  }

  onFocus() {
    const url = this.STIX_Url_Feeds + '/files';
    window.open(url, '_blank');
  }

  downloadStix() {
    this.spinner.show('feeds-spinner');
    this.reporttype = 'application/json';
    this.downloadType = 'Report.json';
    this.tipService.stixReporting()
      .subscribe(
        res => {
          this.response = res;
          window.location.href = this.response.url;
          this.spinner.hide('feeds-spinner');
          this.response = res;
          const file = new Blob([this.response], {
            type: this.reporttype,
          });
          const fileURL = window.URL.createObjectURL(file);
          window.open(fileURL);
          const link = document.createElement('a');
          link.href = fileURL;
          link.download = this.downloadType;
          link.click();
          this.toastr.error('STIX Downloaded Successfully in JSON!');
        },
        err => {
          this.errors = err.status;
          this.response = err;
          this.response = this.response.error.text;
          const file = new Blob([this.response], {
            type: this.reporttype,
          });
          const fileURL = window.URL.createObjectURL(file);
          window.open(fileURL);
          const link = document.createElement('a');
          link.href = fileURL;
          link.download = this.downloadType;
          link.click();
          this.toastr.success('STIX Downloaded Successfully in JSON!');
          this.content = 'STIX Report has been Downloaded';
          this.spinner.hide('feeds-spinner');

          if (this.errors === 400) {
            this.toastr.error(this.errors.error.message);
          } else if (this.errors === 500) {
            this.toastr.error('Oops Problem Occured. Some Problem Occurred while ' +
                'downloading stix! Please Try later');
          } else {
          }
        });
  }

  onNavigateURL(event) {
    window.open(event, '_blank');
  }
}

