import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TipRoutingModule } from './tip-routing.module';
import { TipComponent } from './tip.component';
import { DashboardModule } from './dashboard/dashboard.module';
import {RiskFactorListingComponent} from './riskFactorListing/riskFactorListing.component';
import {TableModule} from 'primeng/table';
import {CardModule} from 'primeng/card';
import {ExploreComponent} from './explore/explore.component';
import {NgxSpinnerModule} from 'ngx-spinner';
import {TooltipModule} from 'primeng/tooltip';
import {TabViewModule} from 'primeng/tabview';
import {ReportPanelComponent} from './Report Panel/reportPanel.component';
import {TopStatisticsComponent} from './Top Statistics/topStatistics.component';
import {NgxEchartsModule} from 'ngx-echarts';
import * as echarts from 'echarts';
import {UrlComponent} from './Top Statistics/url.component';
import {Sha1Component} from './Top Statistics/sha1.component';
import {Sha256Component} from './Top Statistics/sha256.component';
import {Md5Component} from './Top Statistics/md5.component';
import {DomainComponent} from './Top Statistics/domain.component';
import {FeedsComponent} from './feeds/feeds.component';
import {CalendarModule} from 'primeng/calendar';
import {NgxDaterangepickerMd} from 'ngx-daterangepicker-material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MultiSelectModule} from 'primeng/multiselect';
import {AvatarModule} from 'primeng/avatar';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import {ReportModalComponent} from './Report Panel/report-modal/report-modal.component';
import {ReportListComponent} from './Report Panel/report-list/reportList.component';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import { SearchIocComponent } from './searchIoc/searchIoc.component';
import {TagModule} from 'primeng/tag';
import {ChipsModule} from 'primeng/chips';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {ComponentsModule} from '../framework/components/components.module';
import {AuthGuard} from '../core-services/auth.guard';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {TokenInterceptor} from '../core-services/token.interceptor.service';
import {ToastrModule, ToastrService} from 'ngx-toastr';
import {CoreServicesModule} from '../core-services/core-services.module';
import {IocDetailsComponent} from './ioc-details/ioc-details.component';
import {MessageModule} from 'primeng/message';
import {MessagesModule} from 'primeng/messages';
@NgModule({
  declarations: [
    TipComponent, RiskFactorListingComponent, ExploreComponent, SearchIocComponent,
    ReportPanelComponent, IocDetailsComponent,
    TopStatisticsComponent, UrlComponent, Sha1Component, Sha256Component, Md5Component,
    DomainComponent, FeedsComponent, ReportModalComponent, ReportListComponent
  ],

  imports: [
    CommonModule,
    TipRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DashboardModule,
    NgxSpinnerModule,
    ComponentsModule,
    CoreServicesModule,
    TooltipModule,
    TabViewModule,
    TableModule,
    CardModule,
    MessageModule,
    MessagesModule,
    CalendarModule,
    AvatarModule,
    DynamicDialogModule,
    ConfirmDialogModule,
    ChipsModule,
    BreadcrumbModule,
    MultiSelectModule,
    NgxDaterangepickerMd.forRoot(),
    ToastrModule.forRoot(),
    NgxEchartsModule.forRoot({
      echarts,
    }),
    TagModule,
  ],
  providers: [AuthGuard, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true,
  },
    ToastrService],
})
export class TipModule { }
