import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {HttpClient} from '@angular/common/http';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import {TipService} from '../../core-services/tip.service';

@Component({
    selector: 'app-search',
    templateUrl: './searchIoc.component.html',
    styleUrls: ['./searchIoc.component.scss'],
    providers: [DynamicDialogConfig]
})
export class SearchIocComponent implements OnInit {
    loader = false;
    parentMessage;
    showIocPage = false;
    submitSearch = true;
    constructor(private http: HttpClient, private dashboardService: TipService, private spinner: NgxSpinnerService,
                private toastr: ToastrService, protected router: Router, private route: ActivatedRoute) {
      this.showIocPage = false;
      this.route.queryParams.subscribe(params => {
          this.showIocPage = false;
          if (params.submitSearch) {
              this.submitSearch = params.submitSearch;
          }
          this.parentMessage = {
                type: params.type.trim(),
                value: params.value.trim(),
                submitSearch: this.submitSearch
            };
          this.showIocPage = true;
        });
    }

    ngOnInit() {}
}
