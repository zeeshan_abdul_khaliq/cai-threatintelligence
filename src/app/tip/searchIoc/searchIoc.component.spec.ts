import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchIocComponent } from './searchIoc.component';

describe('SearchComponent', () => {
  let component: SearchIocComponent;
  let fixture: ComponentFixture<SearchIocComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchIocComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchIocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
