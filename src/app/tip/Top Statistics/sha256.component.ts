import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {EChartsOption} from 'echarts';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {TipService} from '../../core-services/tip.service';
import {CalendarSettingService} from '../../core-services/calendarSetting.service';
import {IocDetailsComponent} from '../ioc-details/ioc-details.component';

@Component({
    selector: 'app-sha256stats',
    template: `
        <p-card header="Top 10 FileHash-SHA256">
            <div class="spin">
                <ngx-spinner name="sha256-spinner" bdOpacity=0.9 bdColor="#f4f4f9" size="medium" color="#1010ee"
                             type="ball-clip-rotate" [fullScreen]="false"></ngx-spinner>
            </div>
            <div *ngIf="showTable" echarts [options]="tspChart" (chartClick)="openDialog($event)" class="wsh-chart"></div>
            <div *ngIf="hideTable">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <div class="panel panel-default">
                            <p class="msg-dashBoard"> No Record Found </p>
                        </div>
                    </div>
                </div>

            </div>
        </p-card>`,
    styleUrls: ['./topStatistics.component.scss'],
    providers: [DialogService]
})

export class Sha256Component implements OnInit {
    indicator: any;
    count: any;
    showTable: any;
    hideTable: any;
    counturl: any;
    response: any;
    error: any;
    tspChart: EChartsOption = {};

    constructor(private tipService: TipService, private calendarSetting: CalendarSettingService,
                private spinner: NgxSpinnerService, public dialogService: DialogService,
                private router: Router, private toastr: ToastrService) {
    }

    ngOnInit() {
        this.getTopStats('FileHash-SHA256');
    }

    getTopStats(indicator) {
        this.indicator = [];
        this.count = [];
        this.showTable = true;
        this.hideTable = false;
        this.spinner.show('sha256-spinner');
        this.tipService.topIndicator(indicator).subscribe(
            res => {
                this.response = res;
                this.spinner.hide('sha256-spinner');

                if (this.response) {
                    for (let i = 0; i < this.response.length; i++) {
                        this.count.push(this.response[i].count);
                        if (this.response[i].value && this.response[i].value.length > 9) {
                            this.indicator.push(this.response[i].value.substr(0, 8).concat('..'));
                        } else {
                            this.indicator.push(this.response[i].value);
                        }
                    }
                    this.showTable = true;
                    this.hideTable = false;
                    this.createStatsChart(this.response);
                } else {
                    this.showTable = false;
                    this.hideTable = true;
                }
            }, err => {
                this.error = err;
                this.spinner.hide('sha256-spinner');
                this.showTable = false;
                this.hideTable = true;
                if (this.error === 400) {
                    this.toastr.error(this.error.error.message);
                } else {
                    this.toastr.error('Internal Server Error');
                }
            }
        );
    }

    createStatsChart(res) {
        this.tspChart = {
            tooltip: {
                show: true,
                trigger: 'axis',
                formatter(tooltipItem) {
                    let relVal = tooltipItem[0].name;
                    relVal += tooltipItem[0].dataIndex;
                    const c = tooltipItem[0].dataIndex;
                    const value = JSON.stringify(res[c].value);
                    const count = res[c].count;

                    const length = value.length;
                    if (length > 100) {
                        const a = value.substring(0, length / 4);
                        const b = value.substring(length / 4, length / 2);
                        const c = value.substring(length / 2, length);
                        console.log('count', c);
                        return 'Indicator: ' + a + '<br/>' + b + ':=>' + count;
                    } else {
                        return 'Indicator: ' + res[c].value + '<br/>' + 'Count: ' + count;
                    }
                },
                showDelay: 0,
                hideDelay: 50,
                transitionDuration: 0,
                backgroundColor: 'rgba(255,255,255,1)',
                borderColor: '#aaa',
                showContent: true,
                borderWidth: 1,
                // borderRadius: 8,
                padding: 10
            },
            grid: {
                left: 45,
                top: 10,
                right: 0,
                bottom: 40
            },
            xAxis: {
                data: this.indicator,

                axisLabel: {
                    rotate: 30
                },
                axisTick: {
                    alignWithLabel: false
                },
                splitLine: {
                    show: false
                }
            },
            yAxis: {
                type: 'value',

            },
            series: [{
                name: 'Attacking IOC',
                type: 'bar',
                barMaxWidth: 60,
                data: this.count,
                color: '#68CCE3',

            }],
        };

    }

    openDialog(value) {
        let ioc;
        this.response.map((item, i) => {
            if (i === value.dataIndex) {
                ioc = item.value;
                return item;
            }
        });
        this.dialogService.open(IocDetailsComponent, {
            header: 'Indicator Details',
            width: '90%',
            contentStyle: {'min-height': '800px', overflow: 'auto', background: 'white'},
            baseZIndex: 10000,
            data: {type: 'FileHash-SHA256', value: ioc}
        });
    }
}
