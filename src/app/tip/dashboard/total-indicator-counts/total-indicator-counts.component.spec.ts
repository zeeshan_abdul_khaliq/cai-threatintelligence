import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalIndicatorCountsComponent } from './total-indicator-counts.component';

describe('ThreatIntelligenceSourcesComponent', () => {
  let component: TotalIndicatorCountsComponent;
  let fixture: ComponentFixture<TotalIndicatorCountsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TotalIndicatorCountsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalIndicatorCountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
