import { Component, OnInit } from '@angular/core';
import { EChartsOption } from 'echarts';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {Router} from '@angular/router';
import {TipService} from '../../../core-services/tip.service';
import {CalendarSettingService} from '../../../core-services/calendarSetting.service';

@Component({
  selector: 'app-total-indicator-counts',
  templateUrl: './total-indicator-counts.component.html',
  styleUrls: ['./total-indicator-counts.component.scss'],
  providers: [DialogService]
})
export class TotalIndicatorCountsComponent implements OnInit {
  tspChart: EChartsOption = {};
  sbiIocDetailsChart: EChartsOption = {};
  response: any;
  counts: any;
  indicator: any;
  value: any;
  error: any;
  from: any;
  to: any;
  locale: any;
  ranges: any;
  selected: any;
  dataTypes: any;
  showTable = true;
  hideTable = false;
  navigatefirst = false;
  navigate = false;
  navigateC = false;
  navigateIoc = false;
  navigatehash = false;
  navigateIocD = false;
  navigateIocC = false;
  ref: DynamicDialogRef;
  constructor(private dashboardService: TipService, private spinner: NgxSpinnerService, private router: Router,
              private toastr: ToastrService, private caledarSettings: CalendarSettingService, public dialogService: DialogService) { }

  ngOnInit(): void {
    this.locale = this.caledarSettings.locale;
    this.ranges = this.caledarSettings.ranges;
    this.selected = this.caledarSettings.selected;
    this.from  = this.caledarSettings.transformDate(this.selected.startDate);
    this.to  = this.caledarSettings.transformDate(this.selected.endDate);
    this.totalIndictorCountIoc();
  }
  totalIndictorCountIoc() {
    this.indicator = [];
    this.counts = [];
    this.navigate = false;
    this.navigateIoc = false;
    this.navigateC = false;
    this.navigateIocC = false;
    this.navigatehash = false;
    this.navigateIocD = false;

    this.spinner.show('tic-spinner');
    this.dashboardService.gettotalFeedsCount(this.from, this.to).subscribe(
        res => {
          this.spinner.hide('tic-spinner');
          this.response = res;
          if (this.response) {
            this.showTable = true;
            this.navigatefirst = true;
            this.hideTable = false;
            for (let i = 0; i < this.response.length; i++) {
              const data = {
                name: this.response[i].indicatorType,
                value: this.response[i].totalCount
              };
              this.counts.push(data);
              this.indicator.push(this.response[i].indicatorType);
            }
            this.createTICChart(res);
          } else {
            this.showTable = false;
            this.navigatefirst = true;

            this.hideTable = true;

          }
        }, err => {
          this.spinner.hide('tic-spinner');
          this.showTable = false;
          this.navigatefirst = true;

          this.hideTable = true;
          this.toastr.error('Oops some problem occurred. Please wait');
        });
  }
  selectDate() {
  this.from  = this.caledarSettings.transformDate(this.selected.startDate);
  this.to  = this.caledarSettings.transformDate(this.selected.endDate);
  if (this.navigatefirst) {
    this.totalIndictorCountIoc();
  } else if (this.navigateC) {
    this.totalIndictorCountHash();
  }
  }
  totalIndictorCountHash() {
    this.indicator = [];
    this.counts = [];
    this.value = [];

    this.spinner.show('tic-spinner');
    this.dashboardService.gettotalFeedsHashFeedsCount(this.from, this.to).subscribe(
        res => {
          this.spinner.hide('tic-spinner');
          this.response = res;
          if (this.response) {
            this.showTable = false;
            this.hideTable = false;
            this.navigate = true;
            this.navigateIocD = false;
            this.navigatefirst = false;
            this.navigateC = true;
            this.navigateIocC = false;
            for (let i = 0; i < this.response.length; i++) {
              const data = {
                name: this.response[i].indicatorType,
                value: this.response[i].totalCount
              };
              this.counts.push(data);
              this.indicator.push(this.response[i].indicatorType);
            }
            this.createTICChart(res);
          } else {
            this.showTable = false;
            this.hideTable = true;
            this.navigate = false;
            this.navigatefirst = false;
            this.navigateC = true;
            this.navigateIocC = false;
            this.navigateIocD = false;

          }
        }, err => {
          this.spinner.hide('tic-spinner');
          this.showTable = false;
          this.hideTable = true;
          this.navigatefirst = false;
          this.navigateC = true;
          this.navigateIocC = false;
          this.navigate = false;
          this.navigateIocD = false;

          this.toastr.error('Oops some problem occurred. Please wait');
        });
  }
  ticDetails(itemIndex) {
    const l = itemIndex.name;
    this.navigateIoc = true;
    this.navigatehash = false;
    if (itemIndex.name === 'Hash') {
      this.totalIndictorCountHash();
    } else {
      this.dataTypes = itemIndex.name;
      if (this.dataTypes){
        this.router.navigate(['tip/explore'], {queryParams: { type: this.dataTypes }});

      }
    }
  }
  ticHashDetails(itemIndex) {
    this.navigatehash = true;
    this.navigateIoc = false;
    this.dataTypes = itemIndex.name;
    this.router.navigate(['tip/explore'], {queryParams: { type: this.dataTypes}});
  }
  navhashBack() {
    this.navigateIoc = true;
    this.navigatehash = false;
    this.totalIndictorCountHash();
  }
  navIocBack() {
    this.totalIndictorCountIoc();
  }

  createTICChart(res) {
    this.tspChart = {
      tooltip: {
        trigger: 'item',
        formatter(tooltipItem: any) {
          const index = tooltipItem.data;
          if (index.value >= 1000 && index.value <= 99999) {
            index.value = index.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
          } else if (index.value >= 100000 && index.value <= 999999999) {
            index.value = (index.value / 1000000);
            index.value = Math.round(index.value * 100) / 100 + ' M';
          } else if (index.value >= 1000000000 && index.value <= 999999999999) {
            index.value = (index.value / 1000000000);
            index.value = Math.round(index.value * 10) / 10 + ' B';
          }
          const value = index.name.length;
          if (value > 40) {
            const data1 = index.name.substr(0, 39);
            const data2 = index.name.substr(40);
            return 'Indicator: ' + data1 + '<br/>' + data2 + '<br/>' + 'Count: ' + index.value;

          } else {
            return 'Indicator: ' + index.name + '<br/>' + 'Count: ' + index.value;

          }
        },
        showDelay: 0,
        hideDelay: 50,
        transitionDuration: 0,
        backgroundColor: 'rgba(255,255,255,1)',
        borderColor: '#aaa',
        showContent: true,
        borderWidth: 1,
        // borderRadius: 8,
        padding: 10
      },
      grid: {
        left: 0,
        top: 0,
        right: 0,
        bottom: 0
      },
      legend: {

        left: 'left',
        data: this.indicator
      },
      series: [
        {
          type: 'pie',
          radius: '55%',
          center: ['50%', '60%'],
          data: this.counts
        }
      ]
    };

  }
}
