import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreatIntelligenceSourcesComponent } from './threat-intelligence-sources.component';

describe('ThreatIntelligenceSourcesComponent', () => {
  let component: ThreatIntelligenceSourcesComponent;
  let fixture: ComponentFixture<ThreatIntelligenceSourcesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThreatIntelligenceSourcesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreatIntelligenceSourcesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
