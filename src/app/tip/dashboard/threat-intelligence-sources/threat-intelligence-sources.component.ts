import { Component, OnInit } from '@angular/core';
import { EChartsOption } from 'echarts';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {TipService} from '../../../core-services/tip.service';
import {CalendarSettingService} from '../../../core-services/calendarSetting.service';
import {IocDetailsComponent} from '../../ioc-details/ioc-details.component';

@Component({
  selector: 'app-threat-intelligence-sources',
  templateUrl: './threat-intelligence-sources.component.html',
  styleUrls: ['./threat-intelligence-sources.component.scss'],
  providers: [DialogService]
})
export class ThreatIntelligenceSourcesComponent implements OnInit {
  tspChart: EChartsOption = {};
  sbiIocDetailsChart: EChartsOption = {};
  response: any;
  counts: any;
  indicator: any;
  value: any;
  error: any;
  from: any;
  to: any;
  locale: any;
  ranges: any;
  selected: any;
  dataTypes: any;
  showTable = true;
  navigate = false;
  hideTable = false;
  navigateIoc = false;
  navigateIocD = false;
  navigateIocC = false;
  ref: DynamicDialogRef;
  constructor(private dashboardService: TipService, private spinner: NgxSpinnerService,
              private toastr: ToastrService, private caledarSettings: CalendarSettingService,
              public dialogService: DialogService) { }

  ngOnInit(): void {
    this.locale = this.caledarSettings.locale;
    this.ranges = this.caledarSettings.ranges;
    this.selected = this.caledarSettings.selected;
    this.from  = this.caledarSettings.transformDate(this.selected.startDate);
    this.to  = this.caledarSettings.transformDate(this.selected.endDate);
    this.threatIntelligenceSources();
  }
  threatIntelligenceSources() {
    this.indicator = [];
    this.counts = [];
    this.navigate = false;
    this.navigateIoc = false;
    this.navigateIocC = false;
    this.navigateIocD = false;

    this.spinner.show('tiss-spinner');
    this.dashboardService.feedsSourceTypeCount(this.from, this.to).subscribe(
        res => {
          this.response = res;
          if (this.response) {
            this.showTable = true;
            this.navigate = true;
            this.hideTable = false;
            for (let i = 0; i < this.response.length; i++) {
              const data = {
                value: this.response[i].count.toString().split(','),
                name: this.response[i].type
              };
              this.counts.push(data);
            }
            this.dashboardService.feedsCommercialSourcecount(this.from, this.to).subscribe(
                res => {
                  this.spinner.hide('tiss-spinner');

                  this.response = res;
                  if (this.response) {
                    for (let i = 0; i < this.response.length; i++) {
                      if (this.response[i].source === 'T-EYE') {
                        const data = {
                          value: this.response[i].count,
                          name: 'CDP Feeds'
                        };
                        this.indicator.push(data);
                      } else {
                        const data = {
                          value: this.response[i].count,
                          name: this.response[i].source
                        };
                        this.indicator.push(data);
                      }

                    }
                    this.createTISSChart(res);
                  } else {
                    this.indicator = [];
                    this.createTISSChart(res);
                  }
                }, err => {
                  this.spinner.hide('tiss-spinner');
                  this.indicator = [];
                  this.createTISSChart(res);
                });
          } else {
            this.showTable = false;
            this.navigate = true;
            this.spinner.hide('tiss-spinner');

            this.hideTable = true;

          }
        }, err => {
          this.spinner.hide('tiss-spinner');
          this.showTable = false;
          this.navigate = true;

          this.hideTable = true;
          this.toastr.error('Some error occured. Please try again');
        });
  }
  selectDate() {
    this.from  = this.caledarSettings.transformDate(this.selected.startDate);
    this.to  = this.caledarSettings.transformDate(this.selected.endDate);
    if (this.navigate) {
      this.threatIntelligenceSources();
    } else if (this.navigateIocC) {
      this.TIindicators(this.dataTypes);
    }
  }
  TIindicators(itemIndex)  {
      this.indicator = [];
      this.counts = [];
      this.value = [];

      this.spinner.show('tiss-spinner');
      this.dashboardService.OSINTSOURCESCount(this.from, this.to).subscribe(
          res => {
            this.spinner.hide('tiss-spinner');
            this.response = res;
            if (this.response) {
              this.showTable = false;
              this.navigate = false;
              this.navigateIocC = true;
              this.hideTable = false;
              this.navigate = false;
              this.navigateIocD = true;
              for (let i = 0; i < this.response.length; i++) {
                const data = {
                  name: this.response[i].source,
                  value: this.response[i].totalCount
                };
                this.counts.push(data);
              }
              this.createTISSsourcesChart(res);
            } else {
              this.showTable = false;
              this.hideTable = true;
              this.navigate = false;
              this.navigateIocC = true;
              this.navigate = false;
              this.navigateIocD = false;

            }
          },
              err => {
            this.spinner.hide('tiss-spinner');
            this.showTable = false;
            this.hideTable = true;
            this.navigate = false;
            this.navigate = false;
            this.navigateIocC = true;
            this.navigateIocD = false;
            this.toastr.error('Oops some problem occurred. Please wait');
          });

  }
  tissDetails(itemIndex) {
    this.dataTypes = itemIndex.data.name;
    if (this.dataTypes  === 'OSINT') {
      this.navigateIoc = true;
      this.TIindicators(this.dataTypes);

    }
  }
  navIocBack() {
    this.threatIntelligenceSources();
  }

  createTISSChart(res) {
    this.tspChart = {
      tooltip: {
        trigger: 'item',
        formatter(tooltipItem) {
          const relVal = tooltipItem.data.name;
          let index;
          if (relVal === 'KASPERSKY' || relVal === 'CDP Feeds') {
            index = tooltipItem.data.value;
          } else {
            index = tooltipItem.data.value[0];
          }
          if (index >= 1000 && index <= 99999) {
            index = index.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
          } else if (index >= 100000 && index <= 999999999) {
            index = (index / 1000000);
            index = Math.round(index * 100) / 100 + ' M';
          } else if (index >= 1000000000 && index <= 999999999999) {
            index = (index / 1000000000);
            index = Math.round(index * 10) / 10 + ' B';
          }
          return 'Indicator: ' + relVal + '<br/>' + 'Count: ' + index;
        },
        showDelay: 0,
        hideDelay: 50,
        transitionDuration: 0,
        backgroundColor: 'rgba(255,255,255,1)',
        borderColor: '#aaa',
        showContent: true,
        borderWidth: 1,
        padding: 10
      },
      legend: {
        orient: 'vertical',
        left: '70%',
        data: ['Commercial', 'OSINT']
      },
      series: [
        {
          name: 'Attacking IOC ',
          type: 'pie',
          selectedMode: false,
          radius: [0, '35%'],
          label: {
              position: 'inner'
          },
          labelLine: {
              show: false,
            smooth: 0.2,
            length: 3,
            length2: 3
          },
          data: this.indicator,
        },
        {
          name: 'Sources',
          type: 'pie',
          label: {
              formatter: '{b|{b}：}{c}  {per|{d}%}  ',
              padding: [0, 6],

              backgroundColor: '#eee',
              borderColor: '#aaa',
              borderWidth: 1,
              borderRadius: 4,
              rich: {
                a: {
                  color: '#000',
                  lineHeight: 30,
                  align: 'center'
                },
                hr: {
                  borderColor: '#aaa',
                  width: '100%',
                  borderWidth: 0.5,
                  height: 2
                },
                b: {
                  fontSize: 11,
                  lineHeight: 40
                },
                per: {
                  color: '#fff',
                  backgroundColor: '#000',
                  padding: [2, 4],
                  borderRadius: 2
                }
            }
          },
          labelLine: {
            length: 30,
          },
          radius: ['40%', '55%'],
          data: this.counts
        }
      ]
    };

  }
  createTISSsourcesChart(res) {
    this.sbiIocDetailsChart = {
      tooltip: {
        trigger: 'item',
        formatter(info: any) {
          const value = info.value;
          const name = info.name;
          return  'Indicator Type: ' + name + '<br/>' + 'Count: ' + value;

        },
        showDelay: 0,
        hideDelay: 50,
        transitionDuration: 0,
        backgroundColor: 'rgba(255,255,255,1)',
        borderColor: '#aaa',
        showContent: true,
        borderWidth: 1,
        padding: 10
      },

      series: [
        {
          breadcrumb: {
            show: false,
            emptyItemWidth: 0
          },
          type: 'treemap',
          visibleMin: 300,
          label: {
            show: true,
            formatter: '{b}'
          },
          upperLabel: {
            show: true,
            height: 30
          },
          itemStyle: {
            borderColor: '#fff'
          },
          selectedMode: false,
          roam: false,                         // true, false, 'scale' or 'zoom', 'move'
          nodeClick: 'zoomToNode',            // 'zoomToNode', 'link', false
          animation: false,
          data: this.counts
        }
      ]
    };

  }

  openDialog(value) {
    this.ref = this.dialogService.open(IocDetailsComponent, {
      header: 'Indicator Details',
      width: '90%',
      contentStyle: {'min-height': '800px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data:  {type: 'IPv4', value}
    });
  }
}
