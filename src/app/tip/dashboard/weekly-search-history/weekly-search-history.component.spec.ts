import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WeeklySearchHistoryComponent } from './weekly-search-history.component';

describe('WeeklySearchHistoryComponent', () => {
  let component: WeeklySearchHistoryComponent;
  let fixture: ComponentFixture<WeeklySearchHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WeeklySearchHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeeklySearchHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
