import { Component, OnInit } from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {CalendarSettingService} from '../../../core-services/calendarSetting.service';
import {TipService} from '../../../core-services/tip.service';

@Component({
  selector: 'app-weekly-search-history',
  templateUrl: './weekly-search-history.component.html',
  styleUrls: ['./weekly-search-history.component.scss']
})
export class WeeklySearchHistoryComponent implements OnInit {

  bulkSearchResponse: any;
  bulkSearchCounts: any;
  users: any;
  userName: any;
  data: any;
  ipcount: any;
  malwarecount: any;
  urlcount: any;
  otherscount: any;
  ipSOCcount: any;
  malwareSOCcount: any;
  urlSOCcount: any;
  othersSOCcount: any;
  socType: any;
  allcount: any;
  socAdmin = false;
  socNoAdmin = false;
  error: any;
  indicatorData: any;

  ipType: any;
  malwareType: any;
  urlType: any;
  othersType: any;
  constructor(private caledarSettings: CalendarSettingService, private dashboardService: TipService,
              private spinner: NgxSpinnerService, private toastr: ToastrService) {
    this.data = {
      ipCount: 0,
      urlCount: 0,
      malwareCount: 0,
      othersCount: 0,
      allCount: 0,
      ipSocCount: 0,
      urlSocCount: 0,
      malwareSocCount: 0,
      othersSocCount: 0,
      socAdmin: true,
      socNoAdmin: false,
      userName: '',
      bulkSearchCounts: 0
    };
  }
  ngOnInit(): void {
    this.getWeeklySearchCount();
    this.getBulkSearchCounts();
    this.fetchUserName();

  }

  getWeeklySearchCount() {
      this.spinner.show('week-spinner');
      this.urlcount = 0;
      this.malwarecount = 0;
      this.ipcount = 0;
      this.otherscount = 0;
      if (sessionStorage.getItem('role') === 'ROLE_SUPER_ADMIN' || sessionStorage.getItem('role') === 'ROLE_CLIENT_ADMIN' ||
        sessionStorage.getItem('role') === 'ROLE_SOC_ADMIN') {
      this.socAdmin = true;
      this.socNoAdmin = false;
    } else {
      this.socAdmin = false;
      this.socNoAdmin = true;
    }
      this.dashboardService.dashboardheaderCounts(false, null, null).subscribe(
          res => {
            this.allcount = res;
            this.spinner.hide('week-spinner');

            if (this.allcount && this.allcount.length > 0) {
              for (let i = 0; i < this.allcount.length; i++) {
                if (this.allcount [i].type === 'IPv4') {
                  this.ipcount = this.allcount[i].count;
                  this.ipType = this.allcount [i].type;
                  if (this.socAdmin) {
                    this.totalCountIPDetails(this.ipType);
                  }
                  this.ipcount = this.caledarSettings.transformNumbers(this.ipcount);
                } else if (this.allcount [i].type === 'FileHash' || this.allcount[i].type === 'Hash') {
                  this.malwarecount = this.allcount[i].count;
                  this.malwareType = this.allcount [i].type;
                  if (this.socAdmin) {
                    this.totalCountFileHashDetails(this.malwareType);
                  }
                  this.malwarecount = this.caledarSettings.transformNumbers(this.malwarecount);

                } else if (this.allcount [i].type === 'domain' || this.allcount[i].type === 'Domain') {
                  this.urlcount = this.allcount[i].count;
                  this.urlType = this.allcount [i].type;
                  if (this.socAdmin) {
                    this.totalCountDomainDetails(this.urlType);
                  }
                  this.urlcount = this.caledarSettings.transformNumbers(this.urlcount);

                } else if (this.allcount [i].type === 'Other') {
                  this.otherscount = this.allcount[i].count;
                  this.othersType = this.allcount [i].type;
                  if (this.socAdmin) {
                   this.totalCountOthersDetails(this.othersType);
                  }
                  this.otherscount = this.caledarSettings.transformNumbers(this.otherscount);

                }
              }
            }
            this.data.allCount = this.allcount;
            this.data.ipCount = this.ipcount;
            this.data.malwareCount = this.malwarecount;
            this.data.urlCount = this.urlcount;
            this.data.othersCount = this.otherscount;
            this.data.socAdmin = this.socAdmin;
            this.data.socNoAdmin = this.socNoAdmin;

          }, err => {
              this.spinner.hide('week-spinner');
              this.error = err;
              if (this.error === 204) {
              this.data.allCount = this.allcount;
              this.data.ipCount = this.ipcount;
              this.data.malwareCount = this.malwarecount;
              this.data.urlCount = this.urlcount;
              this.data.othersCount = this.otherscount;
              this.data.socAdmin = this.socAdmin;
              this.data.socNoAdmin = this.socNoAdmin;
            }
          });
  }
  fetchUserName() {
    this.dashboardService.fetchUser(sessionStorage.getItem('username')).subscribe(
        res => {
          this.users = res;
          if (this.users !== null) {
            for (let i = 0; i < this.users.length; i++) {
              this.userName = this.users[0].name + '\'s Counts';
              break;
            }
            this.data.userName = this.userName;
          }
        }, err => {
        }
    );
  }
  getBulkSearchCounts() {
    this.bulkSearchCounts = 0;
    this.dashboardService.getBulkSearchCounts().subscribe(res => {
      this.bulkSearchResponse = res;
      if (this.bulkSearchResponse) {
        if (this.bulkSearchResponse.count > 0) {
          this.bulkSearchCounts = this.bulkSearchResponse.count;
        }
      }
      this.data.bulkSearchCounts = this.bulkSearchCounts;

    });
  }
  totalCountIPDetails(type) {
    this.socType = type;
    let counts = 0;
    this.dashboardService.totalCountIOCDetails(this.socType, false, null, null).subscribe(
          res => {
            this.indicatorData = res;
            if (this.indicatorData) {
                if (sessionStorage.getItem('role') === 'ROLE_SUPER_ADMIN' || sessionStorage.getItem('role') === 'ROLE_CLIENT_ADMIN' ||
                    sessionStorage.getItem('role') === 'ROLE_SOC_ADMIN') {
                  this.ipSOCcount = this.indicatorData[this.indicatorData.length - 1].counts;
                  this.data.ipSocCount = this.ipSOCcount;
                } else {
                  counts = 0;
                }
            } else {
              counts = 0;
            }
            }, err => {

            this.error = err;
            this.toastr.error('Oops some problem occurred. Please wait');
          }
      );

  }
  totalCountDomainDetails(type) {
    this.socType = type;
    let counts = 0;
    this.dashboardService.totalCountIOCDetails(this.socType, false, null, null).subscribe(
          res => {
            this.indicatorData = res;
            if (this.indicatorData) {
                if (sessionStorage.getItem('role') === 'ROLE_SUPER_ADMIN' || sessionStorage.getItem('role') === 'ROLE_CLIENT_ADMIN' ||
                    sessionStorage.getItem('role') === 'ROLE_SOC_ADMIN') {
                  counts = this.indicatorData[this.indicatorData.length - 1].counts;
                  this.urlSOCcount = counts;
                  this.data.urlSocCount = this.urlSOCcount;
                } else {
                  counts = 0;
                }
            } else {
              counts = 0;
            }
            }, err => {

            this.error = err;
            this.toastr.error('Oops some problem occurred. Please wait');
          }
      );

  }
  totalCountFileHashDetails(type) {
    this.socType = type;
    let counts = 0;
    this.dashboardService.totalCountIOCDetails(this.socType, false, null, null).subscribe(
          res => {
            this.indicatorData = res;
            if (this.indicatorData) {
                if (sessionStorage.getItem('role') === 'ROLE_SUPER_ADMIN' || sessionStorage.getItem('role') === 'ROLE_CLIENT_ADMIN' ||
                    sessionStorage.getItem('role') === 'ROLE_SOC_ADMIN') {
                  counts = this.indicatorData[this.indicatorData.length - 1].counts;
                  this.malwareSOCcount = counts;
                  this.data.malwareSocCount = this.malwareSOCcount;
                } else {
                  counts = 0;
                }
            } else {
              counts = 0;
            }
            }, err => {

            this.error = err;
            this.toastr.error(this.error.error.message);
          }
      );

  }
  totalCountOthersDetails(type) {
    this.socType = type;
    let counts = 0;
    this.dashboardService.totalCountIOCDetails(this.socType, false, null, null).subscribe(
          res => {
            this.indicatorData = res;
            if (this.indicatorData) {
                if (sessionStorage.getItem('role') === 'ROLE_SUPER_ADMIN' || sessionStorage.getItem('role') === 'ROLE_CLIENT_ADMIN' ||
                    sessionStorage.getItem('role') === 'ROLE_SOC_ADMIN') {
                  this.data.othersSocCount  = this.indicatorData[this.indicatorData.length - 1].counts;

                } else {
                  counts = 0;
                }
            } else {
              counts = 0;
            }
            }, err => {

            this.error = err;
            this.toastr.error('Oops some problem occurred. Please wait');
          }
      );

  }
}

