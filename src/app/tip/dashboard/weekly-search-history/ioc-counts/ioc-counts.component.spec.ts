import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IocCountsComponent } from './ioc-counts.component';

describe('IocCountsComponent', () => {
  let component: IocCountsComponent;
  let fixture: ComponentFixture<IocCountsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IocCountsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IocCountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
