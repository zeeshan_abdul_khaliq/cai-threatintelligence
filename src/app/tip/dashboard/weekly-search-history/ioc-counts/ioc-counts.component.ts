import { Component, OnInit } from '@angular/core';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {TipService} from '../../../../core-services/tip.service';

@Component({
  selector: 'app-ioc-counts',
  templateUrl: './ioc-counts.component.html',
  styleUrls: ['./ioc-counts.component.scss']
})
export class IocCountsComponent implements OnInit {

  indicatorData: any;
  iocType: any;
  constructor(private tipService: TipService, private toastr: ToastrService, private router: Router,
              private spinner: NgxSpinnerService, private route: ActivatedRoute,
              private config: DynamicDialogConfig) {
    if (!this.iocType) {
      this.iocType = config.data.type;
    }
  }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.spinner.show('ioc-spinner');
    this.tipService.totalCountIOCDetails(this.iocType, false, null, null).subscribe(
        res => {
          this.spinner.hide('ioc-spinner');
          this.indicatorData = res;
        }, err =>
        {
          this.spinner.hide('ioc-spinner');
          const error = err;
          this.toastr.error('Oops some problem occurred. Please wait');

        }
    );
  }
}
