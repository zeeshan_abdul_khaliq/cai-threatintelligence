import {Component, Input, OnInit} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import 'd3';
import {TipService} from '../../../core-services/tip.service';
declare var colors: any;
declare var Datamap: any;
declare var d3: any;
@Component({
  selector: 'app-datamap',
  templateUrl: './datamap.component.html',
  styleUrls: ['./datamap.component.scss'],
})
export class DatamapComponent implements OnInit {
  mapData: any;
  option: any ;
  users: any;
  username: any;
  o: any;
  name: any;
  email: any;
  emailId: any;
  role: any;
  createdBy: any;
  createdDate: any;
  error: any;
options: any = {};
  elections: any ;
  election: any;
  constructor(private dashboardService: TipService, private spinner: NgxSpinnerService) {}
  ngOnInit() {
    this.mapData = [];
    this.elections = [];
    this.spinner.show('datamap-spinner');
    this.dashboardService.CountryIndicatorCount().subscribe(
      res => {
        this.o = res;
        this.spinner.hide('datamap-spinner');
        const colors = [
          '#0d552c',
          '#17753f',
          '#289b59',
          '#499b59',
          '#37ba6f',
          '#45cc7f',
          '#58e092',
          '#6ceda3',
          '#93ffc1',
          '#bdffd9',
          '#e7fff1',
        ];
        let prev = 0, color, hits;

        for (let i = 0; i < this.o.length; i++) {
          hits = this.o[i].totalCount;
          if (i <= 9) {
            if (hits !== prev) {
              color = colors[i];
              prev = hits;
            }
          } else {
            color = colors[10];
          }

          let obj = {
            fillColor: color,
            numberOfThings: hits,
            country: this.o[i].country,
            countryCode: this.o[i].countryCode
          };
          this.mapData[this.o[i].countryCode] = obj;
          if (obj.countryCode === 'KOR' || obj.countryCode === 'kor') {
            obj.country = 'South Korea';
          }
          if (obj.countryCode === 'PRK' || obj.countryCode === 'prk') {
            obj.country = 'North Korea';

          }
          this.elections.push(obj);
        }
        this.electionsfunt(this.mapData);
        console.log(this.elections);

        this.election =  new Datamap({
          element: document.getElementById('container1'),
          projection: 'mercator',
          scope: 'world',
          width: 850,
          height: 540,
          options: {
            staticGeoData: false,
            legendHeight: 200,
            legendwidth: 300 // optionally set the padding for the legend
          },

          // fills: {
          //   defaultFill: '#DDDDDD'
          // },
          data: this.mapData,
          geographyConfig: {
            borderColor: '#DEDEDE',
            highlightBorderWidth: 2,
            highlightOnHover: false,
            highlightFillColor(geo) {
                return geo.fillColor || '#0d552c';
              },
            // only change border
            highlightBorderColor: '#B7B7B7',
            popupTemplate(geo, data) {
              if (!data) {
                return ['<div class="hoverinfo">',
                  '<strong>', geo.properties.name, '</strong>',
                  '<br>Count: <strong>', 0, '</strong>',
                  '</div>'].join('');
                
              }
              let count = data.numberOfThings;
              if (count >= 1000 && count <= 99999) {
                count = count.toString().replace(/\B(?=(\d{3})+(?!\d))/g,
                    ',');
              } else if (count >= 100000 && count <= 999999999) {
                count = (count / 1000000);
                count = Math.round(count * 100) / 100 + ' M';
              } else if (count >= 1000000000 && count <= 999999999999) {
                count = (count / 1000000000);
                count = Math.round(count * 10) / 10 + ' B';
              }
              // tooltip content
              return ['<div class="hoverinfo">',
                '<strong>', geo.properties.name, '</strong>',
                '<br>Count: <strong>', count, '</strong>',
                '</div>'].join('');
            }
          }
        });
        console.log(this.election);
        this.election.addPlugin('mylegend', this.addLegend2);
        this.election.mylegend(this.elections);

      },
        err => {
          this.spinner.hide('datamap-spinner');
          let election =  new Datamap({
          element: document.getElementById('container1'),
          projection: 'mercator',

          fills: {defaultFill: '#0d552c'},
          geographyConfig: {
            borderColor: '#DEDEDE',
            highlightBorderWidth: 2,
            highlightFillColor(geo) {
              return geo.fillColor || '#0d552c';
            },
            // only change border
            highlightBorderColor: '#B7B7B7',
            popupTemplate(geo, data) {
              if (!data) {
                return ['<div class="hoverinfo">',
                  '<strong>', geo.properties.name, '</strong>',
                  '<br>Count: <strong>', 0, '</strong>',
                  '</div>'].join('');
                
              }
              // tooltip content
              return ['<div class="hoverinfo">',
                '<strong>', geo.properties.name, '</strong>',
                '<br>Count: <strong>', data.numberOfThings, '</strong>',
                '</div>'].join('');
            }
          }
        });
      });


  }
  electionsfunt(election) {
    this.mapData = election;
  }

   addLegend2(layer, data, options) {
     let html = [],
       label = '';
     let allObj = data;
     this.mapData = data;
     let items = {};
     let count = 0;
     this.mapData.forEach(item => {
       // console.log(item);
       if (count > 9) {
         return;
       }
       let hits = item.numberOfThings;
       let countryname = item.countryCode;

       if (items.hasOwnProperty(hits)) {
         items[hits].push(item);
       } else {
         items[hits] = [item];
         count++;
       }
     });
     let size = Object.keys(items).length;
     // console.log(size);
     for (let key in items) {
       let elem = items[key];
       let color = elem[0].fillColor;
       let width = 100 / size;
       let keys = +key;
       let keyss;
       if (keys >= 1000 && keys <= 99999) {
         keyss = keys.toString().replace(/\B(?=(\d{3})+(?!\d))/g,
           ',');
       } else if (keys >= 100000 && keys <= 999999999) {
         keys = (keys / 1000000);
         keyss = Math.round(keys * 100) / 100 + ' M';
       } else if (keys >= 1000000000 && keys <= 999999999999) {
         keys = (keys / 1000000000);
         keyss = Math.round(keys * 10) / 10 + ' B';
       }
       let htmlStr = '<li class="legend-key" style=" cursor:pointer; width:' + width + '%; text-align: center;padding: 2px; border-top:10px solid ' + color + '; color:#000">';
       htmlStr += keyss;
      // this.createLegend(elem, key);
       let htmler = '';
       if (typeof elem !== 'undefined') {
         htmler = '<div class=\'legend-child\'><b>' + keyss + '</b><ul >';
         elem.forEach(function(item) {
           htmler += '<li>' + item.country + '</li>';
         });
         htmler += '</ul></div>';
       }
       htmlStr += htmler;

       htmlStr += '</li>';
       html[key] = htmlStr;
       // console.log(html);
     }
     // html.reverse();
     d3.select(this.options.element).selectAll('.datamaps-legend').remove();
     d3.select(this.options.element).append('div')
       .attr('class', 'datamaps-legend')
       .html('<ul class="list-inline list-inline-b">' + html.join('') + '</ul>');
  }

}
