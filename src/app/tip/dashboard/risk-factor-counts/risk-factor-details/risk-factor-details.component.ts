import {Component, OnInit, ViewChild} from '@angular/core';
import {Table} from 'primeng/table';
import {Observable} from 'rxjs';
import {DialogService, DynamicDialogConfig, DynamicDialogRef} from 'primeng/dynamicdialog';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {map} from 'rxjs/operators';
import {Angular5Csv} from 'angular5-csv/dist/Angular5-csv';
import {TipService} from '../../../../core-services/tip.service';
import {IocDetailsComponent} from '../../../ioc-details/ioc-details.component';

@Component({
  selector: 'app-risk-factor-details',
  templateUrl: './risk-factor-details.component.html',
  styleUrls: ['./risk-factor-details.component.scss'],
  providers: [DialogService]
})
export class RiskFactorDetailsComponent implements OnInit {
  @ViewChild('dt') dt: Table | undefined;

  exploreData: any;
  type: any;
  typehtml: any;
  filter$: Observable<any>;
  counter = 0;
  totalRecords: any;
  country: any;
  sectorType: any;
  error: any;
  spinnerReport: any = false;
  reporttype: any;
  downloadType: any;
  response: any;
  stixURL: any;
  dataReport: any;
  color: any;
  riskFactorType: any;
  ref: DynamicDialogRef;
    updatedColumns: string[] = ['Type', 'Title', 'Count', 'Country'];
    renderedData: any [];

    constructor(private tipService: TipService, private toastr: ToastrService, private router: Router,
                public dialogService: DialogService, public config: DynamicDialogConfig,
                private spinner: NgxSpinnerService, private route: ActivatedRoute) {
    this.type = config.data.value;
    this.riskFactorType = config.data.value;
    this.counter = 10;
    this.country = null;
    this.sectorType = null;
    this.filter$ = this.route.queryParamMap.pipe(
        map((params: ParamMap) => params.get('type')),
    );
    switch (this.type) {
        case 'Very Low' :
            this.color = 'leads category-color-g';
            break;
        case 'Low' :
            this.color = 'leads category-color-y';
            break;
        case 'Medium' :
            this.color = 'leads category-color-m';
            break;
        case 'High' :
            this.color = 'leads category-color-r';
            break;
        case 'Critical' :
            this.color = 'leads category-color-p';
            break;
        case 'Unknown' :
            this.color = 'leads category-color-n';
            break;
    }

    this.getExplore();
  }
  ngOnInit(): void {
  }

  getExplore() {
    this.spinner.show('explore-spinner');
    this.renderedData = [];
    this.tipService.topRiskFactors(this.riskFactorType, this.counter).subscribe(
        res => {
          this.spinner.hide('explore-spinner');
          this.exploreData = res;
          if (this.exploreData) {
            this.totalRecords = this.exploreData.totalElements;
            this.exploreData = this.exploreData.summaryData;
            for (const exploreObj of this.exploreData) {
                  if (exploreObj.country) {
                      const data = {
                          type: exploreObj.type,
                          title: exploreObj.title,
                          count: exploreObj.count,
                          country: exploreObj.country
                      };
                      this.renderedData.push(data);
                  } else {
                      {
                          const data = {
                              type: exploreObj.type,
                              title: exploreObj.title,
                              count: exploreObj.count,
                              country: '-'
                          };
                          this.renderedData.push(data);
                      }
                  }
              }

          }
        }, err =>
        {
          this.spinner.hide('explore-spinner');
          this.error = err;
          if (this.error === 400) {
            this.toastr.error( this.error.error.message);
          } else  {
              this.toastr.error('Oops some problem occurred. Please wait');
          }
        }
    );
  }

  pagination(event) {
    this.counter  = event.first / 10;
    this.getExplore();
  }

  downloadPdfAttack(type, title) {
    this.reporttype = 'application/pdf';
    this.downloadType = type + ' Report.pdf';
    this.type = type;
    this.spinnerReport = true;
    this.tipService.searchDelve(title, type).subscribe(
        res => {
          this.dataReport = res;
          this.spinnerReport = false;
          this.stixURL = this.dataReport.resource;
          this.tipService.searchReporting(this.dataReport, type)
              .subscribe(
                  res => {
                    this.response = res;
                    this.spinnerReport = false;
                    const file = new Blob([this.response], {
                      type: this.reporttype,
                    });
                    const fileURL = window.URL.createObjectURL(file);
                    const link = document.createElement('a');
                    link.href = fileURL;
                    link.download = this.downloadType;
                    document.body.appendChild(link);
                    link.click();
                    this.toastr.success('Report Download Successful');
                  },
                  err => {
                    this.error = err.status;
                    this.spinnerReport = false;
                    if (this.error === 400) {
                      this.toastr.error( this.error.error.message);
                    } else {
                      this.toastr.error('Oops Some Problem Occurred while ' +
                          'downloading reporting! Please Try later');
                    }
                  });
        },
        err =>  {
          this.error = err.status;
          this.spinnerReport = false;
          if (this.error === 400) {
            this.toastr.error( this.error.error.message);
          } else {
            this.toastr.error('Oops Some Problem Occurred while ' +
                'downloading reporting! Please Try later');
          }
        }
    );
  }
    exportCSV() {
        new Angular5Csv(this.renderedData, 'Downloaded CSV Report', {headers: (this.updatedColumns)});

    }
  downloadStixAttack(type, title) {
    this.spinnerReport = true;
    this.reporttype = 'application/json';
    this.downloadType = type + 'Report.json';
    this.tipService.searchStixReporting(title, type)
        .subscribe(
            res => {
              this.response = res;
              this.spinnerReport = false;
              if (res) {
                const file = new Blob([this.response], {
                  type: this.reporttype,
                });
                const fileURL = window.URL.createObjectURL(file);
                const link = document.createElement('a');
                link.href = fileURL;
                link.download = this.downloadType;
                link.click();
                this.toastr.success('Report Download Successful');
              } else {
                this.toastr.error('No record to Download');
              }
            },
            err => {
              this.spinnerReport = false;
              this.error = err.status;
              if (this.error === 400) {
                this.toastr.error(this.error.error.message);
              } else if (this.error === 404) {
                this.toastr.error('404 Not Found');
              } else if (this.error === 409) {
                this.toastr.error('Conflicted Data');
              } else  {
                this.toastr.error('Oops Some Problem Occurred while downloading reporting! Please Try later');
              }
            });
  }

  openDialog(ioc, type) {
    this.ref = this.dialogService.open(IocDetailsComponent, {
      header: 'Indicator Details',
        width: '90%',
        contentStyle: {'min-height': '600px', overflow: 'auto', background: 'white'},
        baseZIndex: 10000,
      data:  {type, value: ioc}
    });
  }
}

