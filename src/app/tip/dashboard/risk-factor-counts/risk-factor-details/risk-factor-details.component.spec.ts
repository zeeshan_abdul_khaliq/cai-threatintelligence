import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskFactorDetailsComponent } from './risk-factor-details.component';

describe('RiskFactorDetailsComponent', () => {
  let component: RiskFactorDetailsComponent;
  let fixture: ComponentFixture<RiskFactorDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RiskFactorDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskFactorDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
