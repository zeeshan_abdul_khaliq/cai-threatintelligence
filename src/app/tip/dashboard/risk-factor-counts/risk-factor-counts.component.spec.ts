import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskFactorCountsComponent } from './risk-factor-counts.component';

describe('RiskFactorCountsComponent', () => {
  let component: RiskFactorCountsComponent;
  let fixture: ComponentFixture<RiskFactorCountsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RiskFactorCountsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskFactorCountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
