import { Component, OnInit } from '@angular/core';
import { EChartsOption } from 'echarts';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {RiskFactorDetailsComponent} from './risk-factor-details/risk-factor-details.component';
import {TipService} from '../../../core-services/tip.service';

@Component({
  selector: 'app-risk-factor-counts',
  templateUrl: './risk-factor-counts.component.html',
  styleUrls: ['./risk-factor-counts.component.scss'],
  providers: [DialogService]
})
export class RiskFactorCountsComponent implements OnInit {

  rfcChart: EChartsOption = {};
  data: any;
  count: any;
  indicator: any;
  value: any;
  color: any;
  showTable: any = true;
  hideTable: any = false;
  ref: DynamicDialogRef;
  constructor(private dashboardService: TipService, private spinner: NgxSpinnerService,
              private toastr: ToastrService, public dialogService: DialogService) { }

  ngOnInit(): void {
    this.spinner.show('rfc-spinner');
    this.count = [];
    this.indicator = [];
    this.value = [];
    this.color = [];
    this.dashboardService.riskFactor().subscribe(
        res => {
          this.spinner.hide('rfc-spinner');
          this.data = res;
          if (this.data) {
            this.showTable = true;
            this.hideTable = false;
            for (let i = 0; i < this.data.length; i++) {
              this.indicator.push(this.data[i].riskFactor);
              this.count.push(this.data[i].counts);
              const data = {
                value: this.data[i].counts,
                name: this.data[i].riskFactor
              };
              this.value.push(data);
              if (data.name === 'Very Low') {
                this.color.push('#30e012');
              } else if (data.name === 'Low') {
                this.color.push('#e0d42d');

              } else if (data.name === 'Medium') {
                this.color.push('#ef8c04');

              } else if (data.name === 'High') {
                this.color.push('#ef2311');

              } else if (data.name === 'Critical') {
                this.color.push('#a720e7');

              } else if (data.name === 'Unknown') {
                this.color.push('#706969');

              }
            }
            this.rfcChart = {
              tooltip: {
                trigger: 'item',
                formatter(tooltipItem: any) {
                  const index = tooltipItem.dataIndex;
                  if (res[index].counts >= 1000 && res[index].counts <= 99999) {
                    res[index].counts = res[index].counts.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                  } else if (res[index].counts >= 100000 && res[index].counts <= 999999999) {
                    res[index].counts = (res[index].counts / 1000000);
                    res[index].counts = Math.round(res[index].counts * 100) / 100 + ' M';
                  } else if (res[index].counts >= 1000000000 && res[index].counts <= 999999999999) {
                    res[index].counts = (res[index].counts / 1000000000);
                    res[index].counts = Math.round(res[index].counts * 10) / 10 + ' B';
                  }
                  return 'Risk Factor: ' + res[index].riskFactor + '<br/>' + 'Count: ' + res[index].counts;
                },
                showDelay: 0,
                hideDelay: 50,
                transitionDuration: 0,
                backgroundColor: 'rgba(255,255,255,1)',
                borderColor: '#aaa',
                showContent: true,
                borderWidth: 1,
                padding: 10,
                position: [30, 50],
              },
              legend: {
                left: 'left',
                data: this.indicator.sort((a, b) => {
                  return a > b ? 1 : -1;
                }),

              },
              color: this.color,

              series: [
                {
                  type: 'pie',
                  radius: '55%',
                  center: ['57%', '53%'],
                  data: this.value,
                  label: {
                    position: 'outer',
                    alignTo: 'labelLine',
                    bleedMargin: 5
                  },
                  labelLine: {
                    smooth: 0.2,
                    length: 3,
                    length2: 3
                  },
                }
              ],
            };


          } else {
            this.showTable = false;
            this.hideTable = true;
          }

        }, err => {
          this.spinner.hide('rfc-spinner');
          this.showTable = false;
          this.hideTable = true;
          this.toastr.error('Oops some problem occurred. Please wait');
        });
  }

  openDialog(value) {
    this.ref = this.dialogService.open(RiskFactorDetailsComponent, {
      header: 'Indicators',
      width: '90%',
      contentStyle: {'min-height': '500px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data:  {value: value.data.name}
    });
  }
}
