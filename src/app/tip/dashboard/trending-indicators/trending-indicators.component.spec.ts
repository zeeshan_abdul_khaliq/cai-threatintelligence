import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrendingIndicatorsComponent } from './trending-indicators.component';

describe('TrendingIndicatorsComponent', () => {
  let component: TrendingIndicatorsComponent;
  let fixture: ComponentFixture<TrendingIndicatorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrendingIndicatorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrendingIndicatorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
