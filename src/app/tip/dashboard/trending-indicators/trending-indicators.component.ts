import {Component, OnInit} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {TipService} from '../../../core-services/tip.service';
import {IocDetailsComponent} from '../../ioc-details/ioc-details.component';

@Component({
    selector: 'app-trending-indicators',
    templateUrl: './trending-indicators.component.html',
    styleUrls: ['./trending-indicators.component.scss'],
    providers: [DialogService]
})
export class TrendingIndicatorsComponent implements OnInit {
    showTable = true;
    hideTable = false;
    trendingIndicatorList: any;
    error: any;
    ref: DynamicDialogRef;
    constructor(private tipService: TipService, private spinner: NgxSpinnerService,
                private toastr: ToastrService, public dialogService: DialogService) {
    }

    ngOnInit(): void {
        this.spinner.show('ti-spinner');
        this.tipService.topAttackingIOCs().subscribe(
            res => {
                this.spinner.hide('ti-spinner');
                this.trendingIndicatorList = res;
                if (this.trendingIndicatorList) {
                    this.showTable = true;
                    this.hideTable = false;
                } else {
                    this.showTable = false;
                    this.hideTable = true;
                }
            }, err => {
                this.spinner.hide('ti-spinner');
                this.error = err;
                this.showTable = false;
                this.hideTable = true;
                this.toastr.error('Oops some problem occurred. Please wait');

            }
        );
    }

    openDialog(value, type) {
        this.ref = this.dialogService.open(IocDetailsComponent, {
            header: 'Indicator Details',
            width: '90%',
            contentStyle: {'min-height': '800px', overflow: 'auto', background: 'white'},
            baseZIndex: 10000,
            data:  {type, value}
        });
    }

}
