import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DashboardComponent} from './dashboard.component';
import {NgxDaterangepickerMd} from 'ngx-daterangepicker-material';

/*DASHBOARD COMPONENTS*/
import {WeeklySearchHistoryComponent} from './weekly-search-history/weekly-search-history.component';
import {IpvComponent} from './weekly-search-history/ipv/ipv.component';
import {MalwareComponent} from './weekly-search-history/malware/malware.component';
import {UrlComponent} from './weekly-search-history/url/url.component';
import {OthersComponent} from './weekly-search-history/others/others.component';
import {TopStatisticsOfPakistanComponent} from './top-statistics-of-pakistan/top-statistics-of-pakistan.component';
import {TopAttackingIocsPakistanComponent} from './top-attacking-iocs-pakistan/top-attacking-iocs-pakistan.component';
import {SectorBasedIocsComponent} from './sector-based-iocs/sector-based-iocs.component';
import {TotalIndicatorCountsComponent} from './total-indicator-counts/total-indicator-counts.component';
import {RiskFactorCountsComponent} from './risk-factor-counts/risk-factor-counts.component';
import {TrendingIndicatorsComponent} from './trending-indicators/trending-indicators.component';
import {CommercialIndicatorsComponent} from './commercial-indicators/commercial-indicators.component';

/*PRIME NG COMPONENTS*/
import {CardModule} from 'primeng/card';

/*E CHART*/
import {NgxEchartsModule} from 'ngx-echarts';
import * as echarts from 'echarts';
import {FormsModule} from '@angular/forms';
import {NgxSpinnerModule, NgxSpinnerService} from 'ngx-spinner';
import {TooltipModule} from 'primeng/tooltip';
import {DatamapComponent} from './datamap/datamap.component';
import {RouterModule} from '@angular/router';
import {TableModule} from 'primeng/table';
import {ThreatIntelligenceSourcesComponent} from './threat-intelligence-sources/threat-intelligence-sources.component';
import {Ipv4Component} from './weekly-history-details/ipv4/ipv4.component';
import {UrlDomainComponent} from './weekly-history-details/url-domain/url-domain.component';
import {OtherIocComponent} from './weekly-history-details/other-ioc/other-ioc.component';
import {HashesMalComponent} from './weekly-history-details/hashes-mal/hashes-mal.component';
import {TabViewModule} from 'primeng/tabview';
import { RiskFactorDetailsComponent } from './risk-factor-counts/risk-factor-details/risk-factor-details.component';
import { IocCountsComponent } from './weekly-search-history/ioc-counts/ioc-counts.component';

@NgModule({
    declarations: [
        DashboardComponent,
        WeeklySearchHistoryComponent,
        IpvComponent,
        MalwareComponent,
        UrlComponent,
        OthersComponent,
        TopStatisticsOfPakistanComponent,
        TopAttackingIocsPakistanComponent,
        SectorBasedIocsComponent,
        TotalIndicatorCountsComponent,
        RiskFactorCountsComponent,
        TrendingIndicatorsComponent,
        CommercialIndicatorsComponent,
        DatamapComponent,
        ThreatIntelligenceSourcesComponent,
        Ipv4Component,
        UrlDomainComponent,
        OtherIocComponent,
        HashesMalComponent,
        RiskFactorDetailsComponent,
        IocCountsComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        CardModule,
        TableModule,
        RouterModule,
        NgxDaterangepickerMd.forRoot(),
        TooltipModule,
        NgxSpinnerModule,
        NgxEchartsModule.forRoot({
            echarts,
        }),
        TabViewModule,
    ]
})
export class DashboardModule {
}
