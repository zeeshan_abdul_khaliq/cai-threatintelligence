import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UrlDomainComponent } from './url-domain.component';

describe('UrlDomainComponent', () => {
  let component: UrlDomainComponent;
  let fixture: ComponentFixture<UrlDomainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UrlDomainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UrlDomainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
