import { Component, OnInit } from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {DatePipe} from '@angular/common';
import {TipService} from '../../../../core-services/tip.service';
import {IocDetailsComponent} from '../../../ioc-details/ioc-details.component';

@Component({
  selector: 'app-hashes-mal',
  templateUrl: './hashes-mal.component.html',
  styleUrls: ['./hashes-mal.component.scss'],
  providers: [DialogService]
})
export class HashesMalComponent implements OnInit {

  type: any;
  counter = 0;
  constructor(private tipService: TipService, private toastr: ToastrService, private router: Router, private datePipe: DatePipe,
              private spinner: NgxSpinnerService, private route: ActivatedRoute, public dialogService: DialogService) { }
  indicatorData: any;
  malData: any;
  iocType: any;
  ref: DynamicDialogRef;
  malLength: any;
  hashLength: any;
  hashTypeMalware: any;
  hashTypeMd5: any;
  hashTypeSha1: any;
  hashTypeSha256: any;
  startDate: any;
  endDate: any;
  downloadMalwareLoader = false;
  private reporttype: any;
  downloadType: any;
  private eventAttack: any;
  response: any;
  private content: any;
  errors: any;
  ngOnInit(): void {
    this.counter = 0;
    this.type = 'Malware';
    this.getMalwareData();
    this.tipService.getLookupHashFeedsCount().subscribe(res => {
      const o: any = res;
      for (let i = 0; i < o.length; i++) {
        const storeVal = o[i].count;
        if (o[i].type === 'FileHash-MD5') {
          this.hashTypeMd5 = storeVal;
        } else if (o[i].type === 'FileHash-SHA256') {
          this.hashTypeSha256 = storeVal;
        } else if (o[i].type === 'FileHash-SHA1') {
          this.hashTypeSha1 = storeVal;
        }
      }
    }, err => {
      const error = err;
      this.toastr.error(error.error.message);
    });
    this.startDate = new Date();
    this.startDate = this.datePipe.transform(this.startDate.setDate(this.startDate.getDate() - 17), 'yyyy-MM-ddTHH:mm:ss');
    this.endDate = new Date();
    this.endDate = this.datePipe.transform(this.endDate, 'yyyy-MM-ddTHH:mm:ss');
    this.tipService.cuckooMalwareCounts(this.startDate, this.endDate).subscribe(
        res => {
          const malwareRes: any = res;
          if (malwareRes && malwareRes !== null) {
            this.hashTypeMalware = malwareRes.count;
          }
        }, err => {
          const error = err.status;
          this.toastr.error('Oops some problem occurred. Please wait');

        }
    );
  }

  getData(type) {
    this.spinner.show('hash-history-spinner');
    this.tipService.userHistory(type).subscribe(
        res => {
          this.spinner.hide('hash-history-spinner');
          this.indicatorData = res;
          if (res)
          {this.hashLength = this.indicatorData.length; }
        }, err =>
        {
          this.spinner.hide('hash-history-spinner');
          const error = err;
          if (error === 400) {
            this.toastr.error( error.error.message);
          } else {
            this.toastr.error('Oops Problem Occurred. Internal Server Error');
          }
        }
    );
  }

  getMalwareData() {
    this.spinner.show('hash-history-spinner');
    this.tipService.cuckooMalwareList().subscribe(
        res => {
          this.spinner.hide('hash-history-spinner');
          this.malData = res;
          if (res)
          {this.malLength = this.malData.length; }
        }, err =>
        {
          this.spinner.hide('hash-history-spinner');
          const error = err;
          if (error === 400) {
            this.toastr.error( error.error.message);
          } else {
            this.toastr.error('Oops Problem Occurred. Internal Server Error');
          }
        }
    );
  }


  openDialog(value) {
    this.ref = this.dialogService.open(IocDetailsComponent, {
      header: 'Indicator Details',
      width: '90%',
      contentStyle: {'min-height': '600px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data:  {type: this.type, value}
    });
  }

  downloadMalware(cuckooId) {
    // this.loader = true;
    this.downloadMalwareLoader = true;
    this.reporttype = 'application/x-gzip';
    this.downloadType = this.eventAttack + 'Malware-Report.tar.gz';
    this.tipService.cuckooDownloadMalware(cuckooId)
        .subscribe(
            res => {
              // this.loader = false;
              this.downloadMalwareLoader = false;
              this.response = res;
              if (res && res !== null) {
                const file = new Blob([this.response], {
                  type: this.reporttype,
                });
                const fileURL = window.URL.createObjectURL(file);
                const link = document.createElement('a');
                link.href = fileURL;
                link.download = this.downloadType;
                link.click();
                this.toastr.success('Download successful!');
                this.content = 'malware Report has been Downloaded';
                // this.export_loading = false;
                // this.export_reporting = false;
              } else {
                this.toastr.success('No record to Download');
              }

            },
            err => {
              // this.loader = false;
              this.downloadMalwareLoader = false;
              this.errors = err.status;
              if (this.errors === 400) {
                this.toastr.error(this.errors.error.message);
              } else if (this.errors === 404) {
                this.toastr.error('Host Not Found');
              } else if (this.errors === 409) {
                this.toastr.error('Conflicted Data');
              } else  {
                this.toastr.error('Some Problem Occured while ' +
                      'downloading reporting! Please Try later');
              }
              // this.export_loading = false;
              // this.export_reporting = false;
            });
  }

  changeTab(event) {
    this.counter = 0;
    if (event.index === 0) {
      this.type = 'Malware';
      this.getMalwareData();
    } else if (event.index === 1) {
      this.type = 'FileHash-MD5';
      this.getData('FileHash-MD5');
    } else if (event.index === 2){
      this.type = 'FileHash-SHA1';
      this.getData('FileHash-SHA1');
    } else if (event.index === 3) {
      this.type = 'FileHash-SHA256';
      this.getData('FileHash-SHA256');
    }
  }
}
