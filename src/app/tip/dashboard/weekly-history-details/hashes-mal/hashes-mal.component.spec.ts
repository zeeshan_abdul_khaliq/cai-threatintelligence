import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HashesMalComponent } from './hashes-mal.component';

describe('HashesMalComponent', () => {
  let component: HashesMalComponent;
  let fixture: ComponentFixture<HashesMalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HashesMalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HashesMalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
