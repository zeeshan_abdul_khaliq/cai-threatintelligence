import { Component, OnInit } from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {TipService} from '../../../../core-services/tip.service';
import {IocDetailsComponent} from '../../../ioc-details/ioc-details.component';

@Component({
  selector: 'app-ipv4',
  templateUrl: './ipv4.component.html',
  styleUrls: ['./ipv4.component.scss'],
  providers: [DialogService]
})
export class Ipv4Component implements OnInit {
  indicatorData: any;
  iocType: any;
  ref: DynamicDialogRef;
  constructor(private tipService: TipService, private toastr: ToastrService, private router: Router,
              private spinner: NgxSpinnerService, private route: ActivatedRoute, public dialogService: DialogService) {
    this.route.queryParams.subscribe(params => {
      this.iocType = params.type;
    });
  }

  ngOnInit(): void {
    this.getIpv4Data();
  }

  getIpv4Data() {
    this.spinner.show('ip-history-spinner');
    this.tipService.userHistory('IPv4').subscribe(
        res => {
          this.spinner.hide('ip-history-spinner');
          this.indicatorData = res;
        }, err =>
        {
          this.spinner.hide('ip-history-spinner');
          const error = err;
          if (error === 400) {
            this.toastr.error( error.error.message);
          } else {
            this.toastr.error('Oops some problem occurred. Please wait');
          }
        }
    );
  }

  openDialog(value) {
    this.ref = this.dialogService.open(IocDetailsComponent, {
      header: 'Indicator Details',
      width: '90%',
      contentStyle: {'min-height': '800px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data:  {type: 'IPv4', value}
    });
  }
}
