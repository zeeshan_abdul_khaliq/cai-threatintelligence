import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherIocComponent } from './other-ioc.component';

describe('OtherIocComponent', () => {
  let component: OtherIocComponent;
  let fixture: ComponentFixture<OtherIocComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtherIocComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherIocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
