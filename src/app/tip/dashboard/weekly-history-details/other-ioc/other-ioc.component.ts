import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {DialogService} from 'primeng/dynamicdialog';
import {TipService} from '../../../../core-services/tip.service';
import {IocDetailsComponent} from '../../../ioc-details/ioc-details.component';

@Component({
    selector: 'app-other-ioc',
    templateUrl: './other-ioc.component.html',
    styleUrls: ['./other-ioc.component.scss'],
    providers: [DialogService]
})
export class OtherIocComponent implements OnInit {
    type: any;
    counter = 0;
    bulkSearchCount: any;

    constructor(private tipService: TipService, private toastr: ToastrService, private router: Router,
                private spinner: NgxSpinnerService, private route: ActivatedRoute, public dialogService: DialogService) {
    }

    indicatorData: any;
    bulkData: any;
    iocType: any;
    yaraCounts: any;
    cveCounts: any;
    emailCounts: any;
    hostnameCounts: any;

    ngOnInit(): void {
        this.counter = 0;
        this.type = 'Bulk Search';
        this.getCount();
        this.getBulkSearchData();
    }

    getCount() {
        this.tipService.getBulkSearchCounts().subscribe(res => {
            const countsRes: any = res;
            this.bulkSearchCount = 0;
            if (countsRes.count > 0) {
              this.bulkSearchCount = countsRes.count;
            }
        });
        this.tipService.otherIOCTypes().subscribe(res => {
            const allcount: any = res;
            this.emailCounts = 0;
            this.yaraCounts = 0;
            this.cveCounts = 0;
            this.hostnameCounts = 0;
            if (allcount && allcount.length > 0) {
                for (let i = 0; i < allcount.length; i++) {
                    if (allcount[i].type === 'Email') {
                        this.emailCounts = allcount[i].count;
                    } else if (allcount[i].type === 'YARA') {
                        this.yaraCounts = allcount[i].count;
                    } else if (allcount[i].type === 'Hostname') {
                        this.hostnameCounts = allcount[i].count;
                    } else if (allcount[i].type === 'CVE') {
                        this.cveCounts = allcount[i].count;
                    }
                }
            }
        }, err => {
        });
    }

    getData(type) {
        this.spinner.show('other-history-spinner');
        this.tipService.userHistory(type).subscribe(
            res => {
                this.spinner.hide('other-history-spinner');
                this.indicatorData = res;
            }, err => {
                this.spinner.hide('other-history-spinner');
                const error = err;
                this.toastr.error('Oops some problem occurred. Please wait');

            }
        );
    }

    getBulkSearchData() {
        this.spinner.show('other-history-spinner');
        this.tipService.getBulkSearchList().subscribe(
            res => {
                this.spinner.hide('other-history-spinner');
                this.bulkData = res;
            }, err => {
                this.spinner.hide('other-history-spinner');
                const error = err;
                this.toastr.error('Oops some problem occurred. Please wait');

            }
        );
    }


    openDialog(value) {
        this.dialogService.open(IocDetailsComponent, {
            header: 'Indicator Details',
            width: '90%',
            contentStyle: {'min-height': '800px', overflow: 'auto', background: 'white'},
            baseZIndex: 10000,
            data: {type: this.type, value}
        });
    }

    changeTab(event) {
        this.counter = 0;
        if (event.index === 0) {
            this.type = 'Bulk Search';
            this.getBulkSearchData();
        } else if (event.index === 1) {
            this.type = 'Email';
            this.getData('Email');
        } else if (event.index === 2) {
            this.type = 'CVE';
            this.getData('CVE');
        } else if (event.index === 3) {
            this.type = 'YARA';
            this.getData('YARA');
        } else if (event.index === 4) {
            this.type = 'Hostname';
            this.getData('Hostname');
        }
    }
}
