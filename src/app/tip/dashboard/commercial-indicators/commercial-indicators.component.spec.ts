import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommercialIndicatorsComponent } from './commercial-indicators.component';

describe('CommercialIndicatorsComponent', () => {
  let component: CommercialIndicatorsComponent;
  let fixture: ComponentFixture<CommercialIndicatorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommercialIndicatorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommercialIndicatorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
