import { Component, OnInit } from '@angular/core';
import {EChartsOption} from 'echarts';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {TipService} from '../../../core-services/tip.service';
import {IocDetailsComponent} from '../../ioc-details/ioc-details.component';

@Component({
  selector: 'app-commercial-indicators',
  templateUrl: './commercial-indicators.component.html',
  styleUrls: ['./commercial-indicators.component.scss'],
  providers: [DialogService]
})
export class CommercialIndicatorsComponent implements OnInit {
  showTable = true;
  hideTable = false;
  data: any;
  count: any;
  indicator: any = [];
  cinChart: EChartsOption = {};
  error: any;
  ref: DynamicDialogRef;
  constructor(private dashBoardService: TipService, private spinner: NgxSpinnerService,
              private toastr: ToastrService, public dialogService: DialogService) { }

  ngOnInit() {
    this.count = [];
    this.indicator = [];
    this.spinner.show('cin-spinner');
    this.dashBoardService.feedsCommercialIOcs().subscribe(
        res => {
          this.spinner.hide('cin-spinner');
          this.data = res;
          if (this.data) {
            this.showTable = true;
            this.hideTable = false;
            this.indicator.push(['score', 'product']);
            for (let i = 0; i < this.data.length; i++) {
              const data = [this.data[i].count, this.data[i].type];
              this.indicator.push(data);
            }
            this.cinChart = {
              tooltip: {
                show: true,
                trigger: 'axis',

                formatter(tooltipItem) {
                  let relVal = tooltipItem[0].name;
                  relVal += tooltipItem[0].dataIndex;
                  const index = tooltipItem[0].dataIndex;
                  if (res[index].count >= 1000 && res[index].count <= 99999) {
                    res[index].count = res[index].count.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                  } else if (res[index].count >= 100000 && res[index].count <= 999999999) {
                    res[index].count = (res[index].count / 1000000);
                    res[index].count = Math.round(res[index].count * 100) / 100 + ' M';
                  } else if (res[index].count >= 1000000000 && res[index].count <= 999999999999) {
                    res[index].count = (res[index].count / 1000000000);
                    res[index].count = Math.round(res[index].count * 10) / 10 + ' B';
                  }
                  return 'Indicator: ' + res[index].type + '<br/>' + 'Count: ' + res[index].count;
                },
                showDelay: 0,
                hideDelay: 50,
                transitionDuration: 0,
                backgroundColor: 'rgba(255,255,255,1)',
                borderColor: '#aaa',
                showContent: true,
                borderWidth: 1,
                padding: 10,
                axisPointer: {
                  type: 'none'
                }
              },
              dataset: {
                source: this.indicator
              },
              // grid: {containLabel: true},
              xAxis: {
                name: ''
              },
              yAxis: {type: 'category'},
              visualMap: {
                orient: 'horizontal',
                left: 'center',
                min: 100000,
                max: 1000000,
                text: ['Upper Counts', 'Lower Counts'],
                // Map the score column to color
                dimension: 0,
                inRange: {
                  color: ['#D7DA8B', '#E15457']
                }
              },
              series: [
                {
                  type: 'bar',
                  encode: {
                    x: 'score',
                    y: 'product'
                  }
                }
              ]
            };

          } else {
            this.showTable = false;
            this.hideTable = true;
          }
        },
        err => {
          this.spinner.hide('cin-spinner');
          this.showTable = false;
          this.hideTable = true;
          this.toastr.error('Oops some problem occurred. Please wait');

        });
  }

  openDialog(value) {
    this.ref = this.dialogService.open(IocDetailsComponent, {
      header: 'Indicator Details',
      width: '90%',
      contentStyle: {'min-height': '800px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data:  {type: 'IPv4', value}
    });
  }
}
