import { Component, OnInit } from '@angular/core';
import { EChartsOption } from 'echarts';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {Router} from '@angular/router';
import {TipService} from '../../../core-services/tip.service';

@Component({
  selector: 'app-top-statistics-of-pakistan',
  templateUrl: './top-statistics-of-pakistan.component.html',
  styleUrls: ['./top-statistics-of-pakistan.component.scss'],
  providers: [DialogService]
})
export class TopStatisticsOfPakistanComponent implements OnInit {
  showTable = true;
  hideTable = false;
  data: any;
  count: any;
  indicator: any = [];
  tspChart: EChartsOption = {};
  error: any;
  dataTypes: any;
  ref: DynamicDialogRef;
  constructor(private dashBoardService: TipService, private spinner: NgxSpinnerService, private router: Router,
              private toastr: ToastrService, public dialogService: DialogService) { }

  ngOnInit() {
    this.topStatisticInPakistan();

  }
  topStatisticInPakistan() {
    this.indicator = [];
    this.count = [];
    this.spinner.show('stats-spinner');
    this.dashBoardService.attackStatisticsInPakistan('Pakistan').subscribe(
        res => {
          this.spinner.hide('stats-spinner');
          this.data = res;
          if (this.data) {
            this.showTable = true;
            this.hideTable = false;
            for (let i = 0; i < this.data.length; i++) {
              this.indicator.push(this.data[i].indicatorType);
              this.count.push(this.data[i].totalCount);
            }
            this.createChart(res);
          } else {
            this.showTable = false;
            this.hideTable = true;

          }

        }, err => {
          this.spinner.hide('stats-spinner');
          this.showTable = false;
          this.hideTable = true;
          this.toastr.error('Oops some problem occurred. Please wait');
        }
    );
  }
  onChartClick(itemIndex) {
    const l = itemIndex.name;
  }
  openStatsDetails(itemIndex) {
    this.dataTypes = itemIndex.name;
    if (this.dataTypes) {
      this.router.navigate(['tip/explore'], {queryParams: {type: this.dataTypes, country: 'Pakistan'}});

    }
  }
  createChart(res) {
    this.tspChart = {
      tooltip: {
        show: true,
        trigger: 'axis',
        axisPointer: {
          type: 'none'
        },
        formatter(tooltipItem) {
          let relVal = tooltipItem[0].name;
          relVal += tooltipItem[0].dataIndex;
          const index = tooltipItem[0].dataIndex;
          sessionStorage.setItem('DataType', res[index].indicatorType);
          if (res[index].totalCount >= 1000 && res[index].totalCount <= 99999) {
            res[index].totalCount = res[index].totalCount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
          } else if (res[index].totalCount >= 100000 && res[index].totalCount <= 999999999) {
            res[index].totalCount = (res[index].totalCount / 1000000);
            res[index].totalCount = Math.round(res[index].totalCount * 100) / 100 + ' M';
          } else if (res[index].totalCount >= 1000000000 && res[index].totalCount <= 999999999999) {
            res[index].totalCount = (res[index].totalCount / 1000000000);
            res[index].totalCount = Math.round(res[index].totalCount * 10) / 10 + ' B';
          }
          return 'Indicator: ' + res[index].indicatorType + '<br/>' + 'Count: ' + res[index].totalCount;
        },
        showDelay: 0,
        hideDelay: 50,
        transitionDuration: 0,
        backgroundColor: 'rgba(255,255,255,1)',
        borderColor: '#aaa',
        showContent: true,
        borderWidth: 1,
        padding: 10
      },
      grid: {
        left: 45,
        top: 10,
        right: 0,
        bottom: 40
      },
      xAxis: {
        data: this.indicator,

        axisLabel: {
          rotate: 30
        },
        axisTick: {
          alignWithLabel: false
        },
        splitLine: {
          show: false
        }
      },
      yAxis: {
        type: 'value',

      },
      series: [{
        name: 'Attacking IOC',
        type: 'bar',
        barMaxWidth: 60,
        data: this.count,
        color: '#68CCE3',

      }],
    };

  }
}
