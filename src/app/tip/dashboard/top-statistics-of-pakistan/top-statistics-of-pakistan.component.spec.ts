import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopStatisticsOfPakistanComponent } from './top-statistics-of-pakistan.component';

describe('TopStatisticsOfPakistanComponent', () => {
  let component: TopStatisticsOfPakistanComponent;
  let fixture: ComponentFixture<TopStatisticsOfPakistanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopStatisticsOfPakistanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopStatisticsOfPakistanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
