import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopAttackingIocsPakistanComponent } from './top-attacking-iocs-pakistan.component';

describe('TopAttackingIocsPakistanComponent', () => {
  let component: TopAttackingIocsPakistanComponent;
  let fixture: ComponentFixture<TopAttackingIocsPakistanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopAttackingIocsPakistanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopAttackingIocsPakistanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
