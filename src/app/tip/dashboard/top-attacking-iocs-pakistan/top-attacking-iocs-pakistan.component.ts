import { Component, OnInit } from '@angular/core';
import { EChartsOption } from 'echarts';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {TipService} from '../../../core-services/tip.service';
import {IocDetailsComponent} from '../../ioc-details/ioc-details.component';

@Component({
  selector: 'app-top-attacking-iocs-pakistan',
  templateUrl: './top-attacking-iocs-pakistan.component.html',
  styleUrls: ['./top-attacking-iocs-pakistan.component.scss'],
  providers: [DialogService]
})
export class TopAttackingIocsPakistanComponent implements OnInit {
  spin: any = false;
  showTable = true;
  hideTable = false;
  data: any;
  count: any;
  indicator: any;
  value: any;
  userDetail: any = false;
  display = 'none';
  taipChart: EChartsOption = {};
  ref: DynamicDialogRef;
  constructor(private dashboardService: TipService, private spinner: NgxSpinnerService,
              private toastr: ToastrService, public dialogService: DialogService) { }

  ngOnInit() {
    this.spinner.show('taip-spinner');
    this.count = [];
    this.indicator = [];
    this.value = [];
    this.dashboardService.topAttackIOCinPAK('Pakistan').subscribe(
        res => {
          this.spinner.hide('taip-spinner');
          this.data = res;
          if (this.data) {
            this.showTable = true;
            this.hideTable = false;
            for (let i = 0; i < this.data.length; i++) {
              this.indicator.push(this.data[i].value);
              this.count.push(this.data[i].count);
              const data = {
                value: this.data[i].count,
                name: this.data[i].value,
                type: this.data[i].type
              };
              this.value.push(data);
            }
            this.taipChart = {
              tooltip: {
                trigger: 'item',
                formatter(tooltipItem: any) {
                  const index = tooltipItem.dataIndex;
                  if (res[index].count >= 1000 && res[index].count <= 99999) {
                    res[index].count = res[index].count.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                  } else if (res[index].count >= 100000 && res[index].count <= 999999999) {
                    res[index].count = (res[index].count / 1000000);
                    res[index].count = Math.round(res[index].count * 100) / 100 + ' M';
                  } else if (res[index].count >= 1000000000 && res[index].count <= 999999999999) {
                    res[index].count = (res[index].count / 1000000000);
                    res[index].count = Math.round(res[index].count * 10) / 10 + ' B';
                  }
                  return 'Indicator: ' + res[index].type + '<br/>' + 'Count: ' + res[index].count;
                },
                showDelay: 0,
                hideDelay: 50,
                transitionDuration: 0,
                backgroundColor: 'rgba(255,255,255,1)',
                borderColor: '#aaa',
                showContent: true,
                borderWidth: 1,
                padding: 10,
                position: [30, 50],
              },

              grid: {
                left: 0,
                top: 0,
                right: 0,
                bottom: 0
              },
              color: [
                '#68CCE3', '#33406C', '#FFBA53', '#37CE6A'
              ],
              legend: {
                left: 'left',
                data: this.indicator,

              },
              series: [
                {
                  type: 'pie',
                  radius: '55%',
                  center: ['57%', '53%'],
                  data: this.value,
                  label: {
                    position: 'outer',
                    alignTo: 'labelLine',
                    bleedMargin: 5
                  },
                  labelLine: {
                    smooth: 0.2,
                    length: 3,
                    length2: 3
                  },
                }
              ],
            };


          } else {
            this.showTable = false;
            this.hideTable = true;
          }

        }, err => {
          this.spinner.hide('taip-spinner');
          this.showTable = false;
          this.hideTable = true;
          this.toastr.error('Oops some problem occurred. Please wait');

        });


  }


  sendData(event) {
    const data = event.data.name;
    const index = event.dataIndex;
  }

  openDialog(value) {
    let ioc;
    let type;
    this.value.map((item, i) => {
      if (i === value.dataIndex)
      {
        ioc = item.name;
        type = item.type;
        return item;
      }
    });
    this.ref = this.dialogService.open(IocDetailsComponent, {
      header: 'Indicator Details',
      width: '90%',
      contentStyle: {'min-height': '800px', overflow: 'auto', background: 'white'},
      baseZIndex: 10000,
      data:  {type, value: ioc}
    });
  }
}
