import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectorBasedIocsComponent } from './sector-based-iocs.component';

describe('SectorBasedIocsComponent', () => {
  let component: SectorBasedIocsComponent;
  let fixture: ComponentFixture<SectorBasedIocsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectorBasedIocsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectorBasedIocsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
