import { Component, OnInit } from '@angular/core';
import { EChartsOption } from 'echarts';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {Router} from '@angular/router';
import {TipService} from '../../../core-services/tip.service';

@Component({
  selector: 'app-sector-based-iocs',
  templateUrl: './sector-based-iocs.component.html',
  styleUrls: ['./sector-based-iocs.component.scss'],
  providers: [DialogService]
})
export class SectorBasedIocsComponent implements OnInit {
  tspChart: EChartsOption = {};
  sbiHashChart: EChartsOption = {};
  sbiIocDetailsChart: EChartsOption = {};
  response: any;
  counts: any;
  indicator: any;
  value: any;
  error: any;
  dataTypes: any;
  showTable = true;
  hideTable = false;
  navigate = false;
  navigateIoc = false;
  navigatehash = false;
  ref: DynamicDialogRef;
  constructor(private dashboardService: TipService, private spinner: NgxSpinnerService, private router: Router,
              private toastr: ToastrService, public dialogService: DialogService) { }

  ngOnInit(): void {
    this.sectorBaseIoc();
  }
  sectorBaseIoc() {
    this.indicator = [];
    this.counts = [];
    this.navigate = false;
    this.navigateIoc = false;
    this.navigatehash = false;
    this.spinner.show('sbi-spinner');
    this.dashboardService.feedsCountryFinnancialSector('finance', 'Pakistan').subscribe(
        res => {
          this.spinner.hide('sbi-spinner');
          this.response = res;
          if (this.response) {
            this.showTable = true;
            this.hideTable = false;
            for (let i = 0; i < this.response.length; i++) {
              this.indicator.push(this.response[i].type);
              this.counts.push(this.response[i].count);
            }
            this.createSBIChart(res);
          } else {
            this.showTable = false;
            this.hideTable = true;

          }
        }, err => {
          this.spinner.hide('sbi-spinner');
          this.showTable = false;
          this.hideTable = true;
          this.toastr.error('Some error occured. Please try again');
        });
  }
  sectorBaseHash() {
    this.indicator = [];
    this.counts = [];
    this.value = [];

    this.spinner.show('sbi-spinner');
    this.dashboardService.getFinancialHashFeedsCount().subscribe(
        res => {
          this.spinner.hide('sbi-spinner');
          this.response = res;
          if (this.response) {
            this.showTable = false;
            this.hideTable = false;
            this.navigate = true;
            for (let i = 0; i < this.response.length; i++) {
              this.counts.push(this.response[i].totalCount);
              this.indicator.push(this.response[i].indicatorType.substr(0, 8).concat('..'));
              this.value.push(this.response[i].indicatorType);
            }
            this.createSBIHashChart(res);
          } else {
            this.showTable = false;
            this.hideTable = true;
            this.navigate = false;
          }
        }, err => {
          this.spinner.hide('sbi-spinner');
          this.showTable = false;
          this.hideTable = true;

          this.navigate = false;
          this.toastr.error('Oops some problem occurred. Please wait');
        });
  }
  sbiDetails(itemIndex) {
    const l = itemIndex.name;
    this.navigateIoc = true;
    this.navigatehash = false;
    if (itemIndex.name === 'Hash') {
      this.sectorBaseHash();
    } else {
      this.dataTypes = itemIndex.name;
      if (this.dataTypes) {
        this.router.navigate(['tip/explore'], {queryParams: { type: this.dataTypes, sector: 'finance', country: 'Global' }});

      }
    }
  }
  sbiHashDetails(itemIndex) {
    this.navigatehash = true;
    this.navigateIoc = false;
    this.response.map((item, i) => {
      if (i === itemIndex.dataIndex) {
        this.dataTypes = item.indicatorType;
        this.router.navigate(['tip/explore'], {queryParams: { type: this.dataTypes, sector: 'finance', country: 'Global' }});
      }
    });
  }
  navhashBack() {
    this.navigateIoc = true;
    this.navigatehash = false;
    this.sectorBaseHash();
  }
  navIocBack() {
    this.sectorBaseIoc();
  }

  createSBIChart(res) {
    this.tspChart = {
      tooltip: {
        show: true,
        trigger: 'axis',
        formatter(tooltipItem) {
          let relVal = tooltipItem[0].name;
          relVal += tooltipItem[0].dataIndex;
          const index = tooltipItem[0].dataIndex;
          if (res[index].count >= 1000 && res[index].count <= 99999) {
            res[index].count = res[index].count.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
          } else if (res[index].count >= 100000 && res[index].count <= 999999999) {
            res[index].count = (res[index].count / 1000000);
            res[index].count = Math.round(res[index].count * 100) / 100 + ' M';
          } else if (res[index].count >= 1000000000 && res[index].count <= 999999999999) {
            res[index].count = (res[index].count / 1000000000);
            res[index].count = Math.round(res[index].count * 10) / 10 + ' B';
          }
          return 'Indicator: ' + res[index].type + '<br/>' + 'Count: ' + res[index].count;
        },
        showDelay: 0,
        hideDelay: 50,
        transitionDuration: 0,
        backgroundColor: 'rgba(255,255,255,1)',
        borderColor: '#aaa',
        showContent: true,
        borderWidth: 1,
        padding: 10,
        axisPointer: {
          type: 'none'
        }
      },
      grid: {
        left: 45,
        top: 10,
        right: 0,
        bottom: 40
      },
      xAxis: {
        data: this.indicator,

        axisLabel: {
          rotate: 30
        },
        axisTick: {
          alignWithLabel: false
        },
        splitLine: {
          show: false
        }
      },
      yAxis: {
        type: 'value',

      },
      series: [{
        name: 'Attacking IOC',
        type: 'bar',
        barMaxWidth: 60,
        data: this.counts,
        color: '#33406C'

      }],
    };

  }
  createSBIHashChart(res) {
    this.sbiHashChart = {
      tooltip: {
        show: true,
        trigger: 'axis',
        formatter(tooltipItem) {
          let relVal = tooltipItem[0].name;
          relVal += tooltipItem[0].dataIndex;
          const index = tooltipItem[0].dataIndex;
          // sessionStorage.setItem("DataType", res[index].indicatorType);
          if (res[index].totalCount >= 1000 && res[index].totalCount <= 99999) {
            res[index].totalCount = res[index].totalCount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
          } else if (res[index].totalCount >= 100000 && res[index].totalCount <= 999999999) {
            res[index].totalCount = (res[index].totalCount / 1000000);
            res[index].totalCount = Math.round(res[index].totalCount * 100) / 100 + ' M';
          } else if (res[index].totalCount >= 1000000000 && res[index].totalCount <= 999999999999) {
            res[index].totalCount = (res[index].totalCount / 1000000000);
            res[index].totalCount = Math.round(res[index].totalCount * 10) / 10 + ' B';
          }
          return 'Indicator: ' + res[index].indicatorType + '<br/>' + 'Count: ' + res[index].totalCount;
        },
        // position: ['10', '10'],
        showDelay: 0,
        hideDelay: 50,
        transitionDuration: 0,
        backgroundColor: 'rgba(255,255,255,1)',
        borderColor: '#aaa',
        showContent: true,
        borderWidth: 1,
        padding: 10
      },
      grid: {
        left: 45,
        top: 10,
        right: 0,
        bottom: 40
      },
      xAxis: {
        data: this.indicator,

        axisLabel: {
          rotate: 30
        },
        axisTick: {
          alignWithLabel: false
        },
        splitLine: {
          show: false
        }
      },
      yAxis: {
        type: 'value',

      },
      series: [{
        name: 'Attacking IOC',
        type: 'bar',
        barMaxWidth: 60,
        data: this.counts,
        color: '#33406C'

      }],
    };

  }
}
