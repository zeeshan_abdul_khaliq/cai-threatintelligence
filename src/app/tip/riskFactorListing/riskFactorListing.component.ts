import { Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {TipService} from '../../core-services/tip.service';
import {CalendarSettingService} from '../../core-services/calendarSetting.service';
@Component({
  selector: 'app-riskFactorListing',
  templateUrl: './riskFactorListing.component.html',
  styleUrls: ['./riskFactorListing.component.scss'],
})

export class RiskFactorListingComponent implements OnInit {
  requestIncident: any;
  showTable: any;

  constructor(private adminPanel: TipService, private calendarSetting: CalendarSettingService,
              private router: Router) { }

  ngOnInit() {
    this.requestIncident = [];
    this.requestIncident = this.calendarSetting.getriskFactors();
  }
}
