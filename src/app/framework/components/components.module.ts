import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MainHeadingComponent } from './main-heading/main-heading.component';
import { WelcomeHeadingComponent } from './welcome-heading/welcome-heading.component';
import { TextComponent } from './text/text.component';
import { ButtonComponent } from './button/button.component';
import { ButtonModule } from 'primeng/button';
import { InputComponent } from './input/input.component';
import { InputTextModule } from 'primeng/inputtext';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { CheckboxModule } from 'primeng/checkbox';
import { RouterLinkComponent } from './router-link/router-link.component';
import { HeaderComponent } from './header/header.component';
import {AvatarModule} from 'primeng/avatar';
import { MenuComponent } from './menu/menu.component';
import {PanelMenuModule} from 'primeng/panelmenu';
import { PageTitleComponent } from './page-title/page-title.component';
import { RouterLinkImageComponent } from './router-link-image/router-link-image.component';
// import { MenuItem } from 'primeng/api';
@NgModule({
  declarations: [
    MainHeadingComponent,
    WelcomeHeadingComponent,
    TextComponent,
    ButtonComponent,
    InputComponent,
    CheckboxComponent,
    RouterLinkComponent,
    HeaderComponent,
    MenuComponent,
    PageTitleComponent,
    RouterLinkImageComponent
  ],
  imports: [
    CommonModule,
    ButtonModule,
    InputTextModule,
    CheckboxModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    AvatarModule,
    PanelMenuModule,
    // MenuItem
  ],
  exports: [
    MainHeadingComponent,
    WelcomeHeadingComponent,
    TextComponent,
    ButtonComponent,
    InputComponent,
    CheckboxComponent,
    RouterLinkComponent,
    RouterModule,
    HeaderComponent,
    MenuComponent,
    PageTitleComponent,
    RouterLinkImageComponent
  ]
})
export class ComponentsModule { }
