import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';
import {ToastrService} from 'ngx-toastr';
import {TipService} from '../../../core-services/tip.service';
import {CalendarSettingService} from '../../../core-services/calendarSetting.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  items: MenuItem[];
  role: string;
  checkIfdisabeldRoles = true;
  exploreItems: any[];
  exploreResponse: any;

  constructor(private adminPanel: TipService, private toastr: ToastrService, private calendarSettings: CalendarSettingService) {
    this.exploreItems = [];
    this.role = sessionStorage.getItem('role');
    if (this.role === 'Master Soc Analyst' || this.role === 'Distributor Soc Analyst' || this.role === 'Partner Soc Analyst') {
      this.checkIfdisabeldRoles = false;
    }
    this.items = [
      {
        label: 'Home',
        routerLink: '/cai/home',
      },
      {
        label: 'Threat Intelligence Platform',
        styleClass: 'modules',
        items: [
          {
            label: 'Dashboard',
            routerLink: '/tip/dashboard',
            routerLinkActiveOptions: true
          },
          {
            label: 'Top Statistics',
            routerLink: '/tip/top-stats',
            routerLinkActiveOptions: true
          },
          {
            label: 'Explore',
            routerLinkActiveOptions: true,
            items: [],
          },
          {
            label: 'Reports',
            routerLink: '/tip/reports',
            routerLinkActiveOptions: true
          }

        ]
      },
      {
        label: 'Threat Sharing Community',
        styleClass: 'modules tsc',
        icon: 'pi',
        routerLink: '/community/dashboard',

      },
      {
        label: 'Advanced Persistent Threats',
        styleClass: 'modules',
        routerLink: '/apt/dashboard',

      },
      {
        label: 'Infrastructure Monitoring',
        styleClass: 'modules',
        routerLink: '/networkMon/dashboard',

      },
      {
        label: 'Vulnerability Prioritization',
        styleClass: 'modules',
        routerLink: '/vp/dashboard',

      },
      {
        label: 'Attack Surface Monitoring',
        styleClass: 'modules'
      },
      {
        label: 'Distributed Deception Platform',
        styleClass: 'modules',
      },
      {
        label: 'SIEM',
        styleClass: 'modules',
      },
      {
        label: 'SOAR',
      },

      {
        label: 'Third Party Vendors',
        styleClass: 'modules',
        items: [
          {
            label: 'Skurio - DRP ',
            routerLink: '#',
          },
          {
            label: 'RiskXchange - ASM',
            routerLink: '#',
          }

        ]
      },

    ];
  }

  ngOnInit(): void {

    this.getExplore();

  }
  getExplore(): void {
    this.exploreItems = [];
    this.items[1].items[2].items = [];
    this.adminPanel.getFeedsCount().subscribe(
        getFeedsCountRes => {
          this.exploreResponse = getFeedsCountRes;
          if (this.exploreResponse) {
            for (const explore of this.exploreResponse) {
              const item = {
                label: explore.indicatorType + ' (' + this.calendarSettings.transformNumbers(explore.totalCount) + ')',
                routerLink: '/tip/explore',
                queryParams: {type: explore.indicatorType, sector: null, country: null}
              };
              this.exploreItems.push(item);
            }
            this.items[1].items[2].items = this.exploreItems;
          }
        }, getFeedsCountError => {
          this.toastr.error('Oops some problem occurred. Please wait');
        });
  }

}
