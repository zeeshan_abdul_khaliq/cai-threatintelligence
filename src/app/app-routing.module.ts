import { NgModule } from '@angular/core';
import {ExtraOptions, RouterModule, Routes} from '@angular/router';
import {TipModule} from './tip/tip.module';
const routes: Routes = [];
@NgModule({
  imports: [RouterModule.forRoot(routes, {  }), TipModule],
  exports: [RouterModule],
})
export class AppRoutingModule { }
