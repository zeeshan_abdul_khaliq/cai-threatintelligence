import {Injectable} from '@angular/core';
import {ConfigApiService} from './configApi.service';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, retry, tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {ToastrService} from 'ngx-toastr';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  baseURL: string;
  token: string;

  constructor(private config: ConfigApiService, private router: Router, private toastrService: ToastrService
  ) {
    this.baseURL = environment.API_URL;
  }

  intercept(httpRequest: HttpRequest<any>, httpHandler: HttpHandler): Observable<HttpEvent<any>> {
    if (httpRequest.url.includes(this.baseURL + 'reports/generate-report?type=') ||
        httpRequest.url.includes(this.baseURL + 'reports/generate-ioc-report?from=') ||
        httpRequest.url.includes(this.baseURL + 'indicator/malware?type=') ||
        httpRequest.url.includes('/api/cuckoo/submit-malware') ||
        httpRequest.url.includes('/api/lookup/bulk-search') ||
        httpRequest.url.includes('/api/community/collections/file-upload') ||
        httpRequest.url.includes('apt/attack-detail-file-upload') ||
        httpRequest.url.includes('ams/asset-file-upload') ||
        httpRequest.url.includes('apt/technique-detail-file-upload?id=') ||
        httpRequest.url.includes('/api/community/collections/malware-upload?collection=')
        || httpRequest.url.includes('/api/drp/httpRequest-data-take-down')) {
      this.token = sessionStorage.getItem('token');
      httpRequest = httpRequest.clone({
        setHeaders: {
          Authorization: 'Bearer ' + this.token,

        },
      });
      return httpHandler.handle(httpRequest);
    } // token with conten type application/x-www-form-urlencoded
    else if (httpRequest.url === this.baseURL + 'api/vt/submitMalware') {
      this.token = sessionStorage.getItem('token');
      httpRequest = httpRequest.clone({
        setHeaders: {
          'Content-Type': 'application/x-www-form-urlencoded',
          Authorization: 'Bearer ' + this.token,
        },
      });
      return httpHandler.handle(httpRequest).pipe(tap(
          (httpRequestError: any) => {
            if (httpRequestError instanceof HttpResponse) {
              if (httpRequestError.status === 401) {
              }
            }
          }
      ));
    }
    else if (httpRequest.url.includes('/files/')) {
      this.token = sessionStorage.getItem('token');
      httpRequest = httpRequest.clone({
        setHeaders: {
          authorization: this.token,
        },
      });
      return httpHandler.handle(httpRequest).pipe(tap(
          (httpRequestError: any) => {
            if (httpRequestError instanceof HttpResponse) {
              if (httpRequestError.status === 401) {
              }
            }
          }
      ));
    } else {
      this.token = sessionStorage.getItem('token');
      httpRequest = httpRequest.clone({
        setHeaders: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + this.token,
        },
      });
      return httpHandler.handle(httpRequest).pipe(
          catchError((error: HttpErrorResponse) => {
              // handle server-side error
            if (error.status === 401) {
                this.toastrService.error('Session Expired');
                this.router.navigate(['/login']);
              }
            return throwError(error);
          }));
    }
  }
}
