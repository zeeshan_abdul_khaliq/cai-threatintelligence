import {Injectable, OnInit} from '@angular/core';
import {environment} from '../../environments/environment';
@Injectable()
export class ConfigApiService {
    baseUrl: string;
    stixUrlFeeds: string;

    constructor() {
        this.baseUrl = environment.API_URL;
        this.stixUrlFeeds = environment.STIX_Url_Feeds;
    }
    /*-----------------------Adding to Tip Services------------------------------------------*/

    searchDelve(input, type): string {
        const searchUrl = this.baseUrl + 'tip-lkp/lookup/search?input=' + input + '&type=' + type + '&history=false';
        return searchUrl;
    }
    submissionStatus(): string {
        const searchUrl = this.baseUrl + 'tip-lkp/lookup/submitted-requests';
        return searchUrl;
    }
    searchComponent(input, type, refreshValue, historyWeek, apt, submitSearch): string {
        if (!refreshValue) {
            const searchUrl = this.baseUrl + 'tip-lkp/lookup/search?input=' + input + '&type=' + type + '&history=' + historyWeek
                + '&apt=' + apt + '&submitSearch=' + submitSearch;
            return searchUrl;
        } else {
            const searchUrl = this.baseUrl + 'tip-lkp/lookup/search?input=' + input + '&type=' + type + '&intenseScan='
                + refreshValue + '&history=' + historyWeek + '&apt=' + apt + '&submitSearch=' + submitSearch;
            return searchUrl;
        }
    }

    historyDelve(input, type): string {
        const searchUrl = this.baseUrl + 'tip-lkp/lookup/search?input=' + input + '&type=' + type + '&history=true';
        return searchUrl;
    }
    submitmalware(): string {
        const searchUrl = this.baseUrl + 'tip-lkp/lookup/submit-malware';
        return searchUrl;

    }

    cuckooMalwareList(): string {
        const searchUrl = this.baseUrl + 'cuckoo/malware-list';
        return searchUrl;
    }
    fetchUser(user): string {
        const url = this.baseUrl + 'users/user?emailId=' + user;
        return url;
    }
    getBulkSearchList(): string {
        const searchUrl = this.baseUrl + 'tip-lkp/lookup/bulk-searches';
        return searchUrl;
    }

    cuckooMalwareCounts(dateFrom, dateTo): string {
        const searchUrl = this.baseUrl + 'cuckoo/counts';
        return searchUrl;
    }

    cuckooDownloadMalware(id): string {
        const searchUrl = this.baseUrl + 'cuckoo/download?id=' + id;
        return searchUrl;
    }

    searchReporting(attackType): string {
        if (attackType === 'YARA' || attackType === 'CVE' || attackType === 'Hostname' || attackType === 'Email') {
            const report = this.baseUrl + 'reports/generate-rule-report?type=' + attackType;
            return report;
        } else {
            const report = this.baseUrl + 'reports/generate-ioc-report?type=' + attackType;
            return report;
        }
    }

    searchStixReporting(body, attackType): string {
        const report = this.baseUrl + 'stix/get-indicator-stix?indicatorValue=' + body + '&type=' + attackType;
        return report;
    }

    searchAllStixReporting(): string {
        const report = this.stixUrlFeeds + '/download/files/';
        return report;

    }

    getCommunityData(body, attackType): string {
        const report = this.baseUrl + 'community/lookup/tags-by-ioc?value=' + body + '&type=' + attackType;
        return report;
    }

    getAptData(body, attackType): string {
        const report = this.baseUrl + 'apt/lookup/ioc-lookup?ioc=' + body + '&type=' + attackType;
        return report;
    }

    getTipData(body, attackType): string {
        const report = this.baseUrl + 'tip-ds/data-storage/indicator?indicator=' + body + '&type=' + attackType + '&sourceCheck=true';
        return report;
    }
    addschedulereport(event, from, to, type, cruser, enddate, sendtime, org): string {
        const schedulereport = this.baseUrl + 'reports/schedule-report?reportTo=' + to + '&reportFrom=' + from +
            '&userEmail=' + event.emaillist + '&pattern=' + event.selectedfrequencystate + '&endRange=' + enddate +
            '&sendingTime=' + sendtime + '&recurrencePattern=' + event.recurrencepattern + '&organization=' + org +
            '&name=' + cruser;
        return schedulereport;
    }

    putschedulereport(sid, event, from, to, type, cruser, enddate, sendtime, org): string {
        const schedulereport = this.baseUrl + 'reports/schedule-report?reportTo=' + to + '&reportFrom=' + from +
            '&userEmail=' + event.emaillist + '&pattren=' + event.selectedfrequencystate + '&endRange=' + enddate +
            '&sid=' + sid + '&sendingTime=' + sendtime + '&recurrencePattern=' + event.recurrencepattern + '&organization=' + org +
            '&name=' + cruser;
        return schedulereport;
    }

    getschedulereport(user): string {
        const schedulereport = this.baseUrl + 'reports/view-schedule?userName=' + user;
        return schedulereport;
    }

    getCountryList(): string {
        const country = this.baseUrl + 'tip-ds/data-storage/countries-list';
        return country;
    }

    getSectorList(): string {
        const url = this.baseUrl + 'tip-ds/data-storage/sector-list';
        return url;
    }

    getRepotingSectorList(): string {
        const url = this.baseUrl + 'threat-feeds-controller/feeds/sector-list';
        return url;
    }

    postIOC(event): string {
        if (event === 'country') {
            const url = this.baseUrl + 'reports/country-based-ioc-report';
            return url;
        } else if (event === 'sector') {
            const sector = this.baseUrl + 'reports/sector-based-ioc-report';
            return sector;
        }
    }
    iocReport(from, to): string {
        const postReport = this.baseUrl + 'reports/generate-pannel-report?from=' + from + '&to=' + to;
        return postReport;
    }

    iocReportCSV(from, to, indicatorType): string {
        const postReport = this.baseUrl + 'reports/generate-panel-csv-report?from=' + from + '&to=' + to
            + '&indicatorType=' + indicatorType;
        return postReport;
    }
    deletescheduleReport(id, thread): string {
        const schedulereport = this.baseUrl + 'reports/delete-schedule?sid=' + id + '&currentThread=' + thread;
        return schedulereport;
    }
    topIPs(): string {
        const ip = this.baseUrl + 'tip-ds/data-storage/top-ips';
        return ip;
    }

    topDomains(): string {
        const domain = this.baseUrl + 'tip-ds/data-storage/top-domains';
        return domain;
    }

    topYaras(): string {
        const yara = this.baseUrl + 'tip-ds/data-storage/top-domains';
        return yara;
    }

    topEmails(): string {
        const email = this.baseUrl + 'tip-ds/data-storage/top-domains';
        return email;
    }

    topHostings(): string {
        const hosting = this.baseUrl + 'tip-ds/data-storage/top-domains';
        return hosting;
    }

    topCves(): string {
        const cve = this.baseUrl + 'tip-ds/data-storage/top-domains';
        return cve;
    }

    topAttackingIOCs(): string {
        const attackingIOCs = this.baseUrl + 'tip-ds/data-storage/top-attacking-iocs';
        return attackingIOCs;
    }

    topCategories(indicator, size): string {
        const topCategories = this.baseUrl + 'tip-ds/data-storage/all-indicators?category=' + indicator + '&page=0&size=' + size + '&orderBy=DESC';
        return topCategories;
    }

    topRiskFactors(indicator, size): string {
        const topCategories = this.baseUrl + 'tip-ds/data-storage/all-indicators?indicatorType=null&riskFactor=' + indicator +
            '&page=' + size + '&size=10&orderBy=DESC';
        return topCategories;
    }

    topMalwaresMd5(): string {
        const malware = this.baseUrl + 'tip-ds/data-storage/top-malwares?malwareType=md5';
        return malware;
    }

    topMalwaresSha1(): string {
        const malware = this.baseUrl + 'tip-ds/data-storage/top-malwares?malwareType=sha1';
        return malware;
    }

    topMalwaresSha256(): string {
        const malware = this.baseUrl + 'tip-ds/data-storage/top-malwares?malwareType=sha256';
        return malware;
    }

    topIndicator(indicator): string {
        const url = this.baseUrl + 'tip-ds/data-storage/top-indicators?indicatorType=' + indicator;
        return url;
    }

    topTotalFeedIndicator(indicator, from, to): string {
        const url = this.baseUrl + 'tip-ds/data-storage/top-indicators?indicatorType=' + indicator + '&from=' + from + '&to=' + to;
        return url;
    }

    getFeedsCount(): string {
        const url = this.baseUrl + 'tip-ds/data-storage/all-indicator-type-count';
        return url;
    }

    gettotalFeedsCount(from, to): string {
        const url = this.baseUrl + 'tip-ds/data-storage/all-indicator-type-count?from=' + from + '&to=' + to;
        return url;
    }

    gettotalFeedsHashFeedsCount(from, to): string {
        const url = this.baseUrl + 'tip-ds/data-storage/hash-indicator-type-count?from=' + from + '&to=' + to;
        return url;
    }

    getHashFeedsCount(): string {
        const url = this.baseUrl + 'tip-ds/data-storage/hash-indicator-type-count';
        return url;
    }

    getFinancialHashFeedsCount(): string {
        const url = this.baseUrl + 'tip-ds/data-storage/hash-indicator-type-count?sector=Finance';
        return url;
    }

    getLookupHashFeedsCount(): string {
        const url = this.baseUrl + 'tip-lkp/lookup/hash-counts';
        return url;
    }

    riskFactor(): string {
        const url = this.baseUrl + 'tip-ds/data-storage/risk-factor-counts';
        return url;
    }

    getAllFeeds(indicatorType, page, type, country): string {
        console.log(type, country);
        if (type === null && country !== null) {
            const url = this.baseUrl + 'tip-ds/data-storage/all-indicators?indicatorType=' + indicatorType +
                '&country=Pakistan&page=' + page + '&size=10&orderBy=desc';
            return url;
        } else if (type !== null && country !== null) {
            const url = this.baseUrl + 'tip-ds/data-storage/all-indicators?indicatorType=' + indicatorType
                + '&sector=Finance&page=' + page + '&size=10&orderBy=desc';
            return url;
        } else if (type !== null && country === null) {
            const url = this.baseUrl + 'tip-ds/data-storage/all-indicators?indicatorType=' + indicatorType +
                '&sector=Finance&page=' + page + '&size=10&orderBy=desc';
            return url;
        } else {
            const url = this.baseUrl + 'tip-ds/data-storage/all-indicators?indicatorType=' + indicatorType +
                '&page=' + page + '&size=10&orderBy=desc';
            return url;
        }
    }

    getFeedDetail(indicator, type): string {
        const url = this.baseUrl + 'tip-ds/data-storage/indicator?indicator=' + indicator + '&type=' + type;
        return url;
    }

    totalCountIOC(): string {
        const url = this.baseUrl + 'tip-lkp/lookup/total-count';
        return url;
    }

    totalCountIOCDetails(type, date, from, to): string {
        if (date) {
            const url = this.baseUrl + 'tip-lkp/lookup/total-count-details?type=' + type + '&date=true&dateFrom=' + from + '&dateTo=' + to;
            return url;
        } else {
            const url = this.baseUrl + 'tip-lkp/lookup/total-count-details?type=' + type + '&date=false';
            return url;
        }
    }
    weeklyStats(type): string {
        const url = this.baseUrl + 'tip-lkp/lookup/weekly-stats?type=' + type;
        return url;
    }

    iocDates(input, type): string {
        const url = this.baseUrl + 'tip-lkp/lookup/ioc-dates?input=' + input + '&type=' + type;
        return url;
    }

    otherIOCTypes(): string {
        const url = this.baseUrl + 'tip-lkp/lookup/other-ioctypes';
        return url;
    }

    getBulkSearchCounts(): string {
        const url = this.baseUrl + 'tip-lkp/lookup/bulk-search-count';
        return url;
    }

    userHistory(type): string {
        const url = this.baseUrl + 'tip-lkp/lookup/user-history?type=' + type;
        return url;
    }

    userHistoryDashboard(page, type): string {
        const url = this.baseUrl + 'tip-lkp/lookup/user-history?type=' + type;
        return url;
    }

    dashboardheaderCounts(date, from, to): string {

        if (date) {
            const url = this.baseUrl + 'tip-lkp/lookup/total-count?date=true&dateFrom=' + from + '&dateTo=' + to;
            return url;
        } else {
            const url = this.baseUrl + 'tip-lkp/lookup/total-count';
            return url;
        }
    }


// TIPDAShboard Charts & MAP
    CountryIndicatorCount(): string {
        const url = this.baseUrl + 'tip-ds/data-storage/country-indicator-count';
        return url;
    }

// if dropdown is destination
    countryattackDestination(): string {
        const url = this.baseUrl + 'tip-ds/data-storage/attack-destinations';
        return url;
    }

    countryattackSources(country, countryCode, lat, long): string {
        const url = this.baseUrl + 'tip-ds/data-storage/attack-sources?country=' + country + '&countryCode3=' +
            countryCode + '&latitude=' + lat + '&longitude=' + long;
        return url;
    }

// if dropdown is sources
    countrySources(): string {
        const url = this.baseUrl + 'tip-ds/data-storage/attack-by-sources';
        return url;
    }

    countryArcDestiantion(country, countryCode, lat, long): string {
        const url = this.baseUrl + 'tip-ds/data-storage/attack-on-destinations?country=' + country + '&countryCode3=' +
            countryCode + '&latitude=' + lat + '&longitude=' + long;
        return url;
    }

    attackStatisticsInPakistan(country): string {
        const url = this.baseUrl + 'tip-ds/data-storage/all-indicator-type-count?country=' + country;
        return url;
    }

    topAttackIOCinPAK(country): string {
        const url = this.baseUrl + 'tip-ds/data-storage/top-attacking-iocs?country=' + country;
        return url;
    }

    feedsSourceTypeCount(from, to): string { // eg osint and commercial
        const url = this.baseUrl + 'tip-ds/data-storage/source-type-count?from=' + from + '&to=' + to;
        return url;
    }

    OSINTSOURCESCount(from, to): string { // eg osint
        const url = this.baseUrl + 'tip-ds/data-storage/osint-sources-count?from=' + from + '&to=' + to;
        return url;
    }

    feedsCommercialSourcecount(from, to): string { // eg teye and kasperskey
        const url = this.baseUrl + 'tip-ds/data-storage/commercial-sources-count?from=' + from + '&to=' + to;
        return url;
    }

    feedsCommercialIOcs(): string {
        const url = this.baseUrl + 'tip-ds/data-storage/commercial-indicator-type-count';
        return url;
    }

    feedsFinnancialSector(sector): string {
        const url = this.baseUrl + 'tip-ds/data-storage/sector-count?sector=' + sector;
        return url;
    }

    feedsCountryFinnancialSector(sector, country): string {
        const url = this.baseUrl + 'tip-ds/data-storage/sector-indicator-type-count?sector=' + sector;

        return url;
    }

    feedsTopCountryFinnancialSector(type, country): string {
        const url = this.baseUrl + 'tip-ds/data-storage/top-sector-indicators?indicatorType=' + type + '&sector=Finance';
        return url;
    }

    feedsTopCountryDetails(type): string {
        const url = this.baseUrl + 'tip-ds/data-storage/top-country-indicators?indicatorType=' + type + '&country=Pakistan';
        return url;
    }


}
