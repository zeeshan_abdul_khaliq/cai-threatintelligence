import {Injectable, OnInit} from '@angular/core';
import {ConfigApiService} from './configApi.service';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {GetExploreAllFeeds, HashIndicatorCounts} from './tip-interfaces';
const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'}),
    responseType: 'arraybuffer' as 'arraybuffer',
};
const httpOptionss = {
    headers: new HttpHeaders({'Content-Type': 'application/json'}),
};
const httpOptionsss = {
    headers: new HttpHeaders({'Content-Type': 'text/plain'}),
};
@Injectable()
export class TipService {
    file: Set<File> = new Set();
    info: any = null;
    private storageDrpSub = new Subject<boolean>();
    constructor(private http: HttpClient, private configApi: ConfigApiService) {
    }
    watchStorage(): Observable<any> {
        return this.storageDrpSub.asObservable();
    }

    saveitem(data): any {
        this.storageDrpSub.next(true);
    }
    weeklyStats(type): any {
        return this.http.get(this.configApi.weeklyStats(type));
    }

    iocDates(input, type): any {
        return this.http.get(this.configApi.iocDates(input, type));
    }

    cuckooMalwareCounts(dateFrom, dateTo): any {
        return this.http.get(this.configApi.cuckooMalwareCounts(dateFrom, dateTo));
    }

    getLookupHashFeedsCount(): any {
        return this.http.get(this.configApi.getLookupHashFeedsCount());
    }

    userHistory(type): any {
        const url = this.configApi.userHistory(type);
        return this.http.get(url);
    }
    userHistoryDashboard(page, type): any {
        const url = this.configApi.userHistoryDashboard(page, type);
        return this.http.get(url);
    }
    dashboardheaderCounts(date, from, to): any {
        const url = this.configApi.dashboardheaderCounts(date, from, to);
        return this.http.get(url);
    }

    otherIOCTypes(): any {
        const url = this.configApi.otherIOCTypes();
        return this.http.get(url);
    }

    getBulkSearchCounts(): any {
        const url = this.configApi.getBulkSearchCounts();
        return this.http.get(url);
    }

    getBulkSearchList(): any {
        const url = this.configApi.getBulkSearchList();
        return this.http.get(url);
    }


    // For Dahsboard Charts & MAP
    CountryIndicatorCount(): any {
        const url = this.configApi.CountryIndicatorCount();
        return this.http.get(url);
    }
// if dropdown is destination

    countryattackDestination(): any {
        const url = this.configApi.countryattackDestination();
        return this.http.get(url);
    }

    countryattackSources(country, countryCode, lat, long): any {
        const url = this.configApi.countryattackSources(country, countryCode, lat, long);
        return this.http.get(url);
    }
// if dropdown is sources
    countrySources(): any {
        const url = this.configApi.countrySources();
        return this.http.get(url);
    }

    countryArcDestiantion(country, countryCode, lat, long): any {
        const url = this.configApi.countryArcDestiantion(country, countryCode, lat, long);
        return this.http.get(url);
    }
    attackStatisticsInPakistan(country): any {
        const url = this.configApi.attackStatisticsInPakistan(country);
        return this.http.get(url);
    }

    topAttackIOCinPAK(country): any {
        const url = this.configApi.topAttackIOCinPAK(country);
        return this.http.get(url);
    }

    feedsSourceTypeCount(from, to): any { // eg osint and Commercail
        const url = this.configApi.feedsSourceTypeCount(from, to);
        return this.http.get(url);
    }
    OSINTSOURCESCount(from, to): any{ // eg osint
        const url = this.configApi.OSINTSOURCESCount(from, to);
        return this.http.get(url);
    }

    feedsCommercialSourcecount(from, to): any { // eg teye and kasperskey
        const url = this.configApi.feedsCommercialSourcecount(from, to);
        return this.http.get(url);
    }

    feedsCommercialIOcs(): any {
        const url = this.configApi.feedsCommercialIOcs();
        return this.http.get(url);
    }

    feedsFinnancialSector(sector): any {
        const url = this.configApi.feedsFinnancialSector(sector);
        return this.http.get(url);
    }

    feedsCountryFinnancialSector(sector, country): any {
        const url = this.configApi.feedsCountryFinnancialSector(sector, country);
        return this.http.get(url);
    }
    riskFactor(): any {
        const url = this.configApi.riskFactor();
        return this.http.get(url);
    }
    topAttackingIOCs(): any {
        const url = this.configApi.topAttackingIOCs();
        return this.http.get(url);
    }
    feedsTopCountryFinnancialSector(sector, country): any {
        const url = this.configApi.feedsTopCountryFinnancialSector(sector, country);
        return this.http.get(url);
    }
    getFinancialHashFeedsCount(): any {
        const url = this.configApi.getFinancialHashFeedsCount();
        return this.http.get(url);
    }
    feedsTopCountryDetails(sector): any {
        const url = this.configApi.feedsTopCountryDetails(sector);
        return this.http.get(url);
    }
    totalCountIOC(): any { // lookup/total-count
        const url = this.configApi.totalCountIOC();
        return this.http.get(url);
    }

    totalCountIOCDetails(type, date, from, to): any { // lookup/total-count
        const url = this.configApi.totalCountIOCDetails(type, date,  from, to);
        return this.http.get(url);
    }
    fetchUser(username: any): any {
        const url = this.configApi.fetchUser(username);
        return this.http.get(url);
    }
    topRiskFactors(indicator, size): any {
        return this.http.get(this.configApi.topRiskFactors(indicator, size));
    }
    cuckooMalwareList(): any {
        const url = this.configApi.cuckooMalwareList();
        return this.http.get(url);
    }

    searchStixReporting(body, attackType): any {
        const options = {responseType: 'blob' as 'json'};
        const url = this.configApi.searchStixReporting(body, attackType);
        return this.http.get(url, options);
    }

    saveMalware(): any {
        return this.file;
    }

    searchReporting(body, attackType): any {
        const options = {responseType: 'blob' as 'json'};
        const url = this.configApi.searchReporting(attackType);
        return this.http.post<Blob>(url, body, options);
    }

    searchComponent(input, type, refreshValue, historyWeek, apt, submitSearch): any {
        const iocResponse = this.configApi.searchComponent(input, type, refreshValue, historyWeek, apt, submitSearch);
        return this.http.get(iocResponse);
    }
    getAptData(body, attackType): any {
        const url = this.configApi.getAptData(body, attackType);
        return this.http.get(url);
    }
    cuckooDownloadMalware(id): any {
        const options = {responseType: 'blob' as 'json'};
        const url = this.configApi.cuckooDownloadMalware(id);
        return this.http.get(url, options);
    }
    gettotalFeedsCount(from, to): any {
        return this.http.get(this.configApi.gettotalFeedsCount(from, to));
    }
    gettotalFeedsHashFeedsCount(from, to): any {
        return this.http.get( this.configApi.gettotalFeedsHashFeedsCount(from, to));
    }
    topTotalFeedIndicator(indicator, from, to): any {
        return this.http.get(this.configApi.topTotalFeedIndicator(indicator, from, to));
    }
    // Feeds
    stixReporting(): Observable<HttpResponse<any>> {
        const url = this.configApi.searchAllStixReporting();
        return this.http.post<any>(url, null);
    }
    // Top Statistics
    topIndicator(indicator): any {
        return this.http.get(this.configApi.topIndicator(indicator));
    }
    // Explore
    getAllFeeds(indicatorType, size, type, country): any {
        return this.http.get(this.configApi.getAllFeeds(indicatorType, size, type, country));
    }
    getexploreAllFeeds(indicatorType, size, type, country): Observable<GetExploreAllFeeds> {
        return this.http.get<GetExploreAllFeeds>(this.configApi.getAllFeeds(indicatorType, size, type, country));
    }
    getFeedsCount(): any {
        return this.http.get(this.configApi.getFeedsCount());
    }
    getHashFeedsCount(): Observable<HashIndicatorCounts[]> {
        return this.http.get<HashIndicatorCounts[]>(this.configApi.getHashFeedsCount());
    }
    // Search
    searchDelve(body, type): any {
        return this.http.get(this.configApi.searchDelve(body, type));
    }
    submissionStatus(): any  {
        const url = this.configApi.submissionStatus() ;
        return this.http.get(url);
    }

    getFeedDetail(indicator, type): any {
        return this.http.get(this.configApi.getFeedDetail(indicator, type));
    }
    // Reporting
    iocReport(data, from, to): any {
        const options = {responseType: 'blob' as 'json'};
        const api = this.configApi.iocReport(from, to);
        return this.http.post(api, data, options);
    }
    iocReportCSV(data, from, to): any {
        const options = {responseType: 'blob' as 'json'};
        const api = this.configApi.iocReportCSV(from, to, data);
        return this.http.get(api, options);
    }
    addscheduleReport(user, event, from, to, type, cruser, enddate, sendtime, org): any {
        const api = this.configApi.addschedulereport(event, from, to, type, cruser, enddate, sendtime, org);
        return this.http.post(api, user, httpOptions);
    }
    putscheduleReport(user, event, from, to, type, cruser, enddate, sendtime, org): any {
        const api = this.configApi.putschedulereport(user.sid, event, from, to, type, cruser, enddate, sendtime, org);
        return this.http.put(api, user, httpOptions);
    }
    getscheduleReport(user): any {
        const api = this.configApi.getschedulereport(user);
        return this.http.get(api);
    }
    getSectorList(): any {
        const api = this.configApi.getSectorList();
        return this.http.get(api);
    }
    getCountryList(): any {
        const api = this.configApi.getCountryList();
        return this.http.get(api);
    }

    postIOC(data, category): any {
        const options = {responseType: 'blob' as 'json'};
        const api = this.configApi.postIOC(category);
        if (api) {
            return this.http.post(api, data, options);
        }
    }

    deletescheduleReport(id, thread): any {
        const api = this.configApi.deletescheduleReport(id, thread);
        return this.http.delete(api, httpOptionsss);
    }
// foor extra work
    getForAllDates(data): any {

        this.info = data;
    }

    returndates(): any {
        if (this.info === null) {
            return null;
        } else {
            return this.info;
        }
    }

}
